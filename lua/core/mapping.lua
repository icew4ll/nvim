local opts = { noremap = true, silent = false }

local generic_opts = {
	insert_mode = opts,
	normal_mode = opts,
	visual_mode = opts,
	visual_block_mode = opts,
	command_mode = opts,
	term_mode = { silent = false },
}

local mode_adapters = {
	insert_mode = "i",
	normal_mode = "n",
	term_mode = "t",
	visual_mode = "v",
	visual_block_mode = "x",
	command_mode = "c",
	onore_mode = "o",
}

local keymappings = {
	insert_mode = {
		["jk"] = "<Esc>",
	},
	normal_mode = {
		-- window movement
		["<C-h>"] = "<C-w>h",
		["<C-j>"] = "<C-w>j",
		["<C-k>"] = "<C-w>k",
		["<C-l>"] = "<C-w>l",
		["<Esc>"] = ":noh<CR>",
		["<Tab>"] = ":bnext<CR>",
		["<S-Tab>"] = ":bprevious<CR>",
		["bn"] = ":bnext<CR>",
		["bp"] = ":bprevious<CR>",
		["ty"] = ":SymbolsOutline<CR>",
		["tr"] = ":TroubleToggle document_diagnostics<CR>",
		["ta"] = ":qa<CR>",
		["tl"] = ":Telescope<CR>",
		["tb"] = ":Telescope buffers<CR>",
		["to"] = ":Telescope oldfiles<CR>",
		["tm"] = ":lua require('telescope').extensions.media_files.media_files()<CR>",
		["t,"] = ":VtrAttachToPane<CR>",
		["t."] = ":VtrSendLinesToRunner<CR>",
		["td"] = ":bdelete<CR>",
		["tn"] = ":Nnn<CR>",
		["tf"] = ":lua vim.lsp.buf.formatting()<CR>",
		["te"] = ":.!eslint --fix<CR>",
		["tu"] = ":PackerUpdate<CR>",
		["tc"] = ":PackerCompile<CR>",
		["tq"] = ":q<CR>",
		["ts"] = ":w<CR>",
	},
	onore_mode = {
		["iu"] = ':<c-u>lua require"treesitter-unit".select()<CR>',
		["au"] = ':<c-u>lua require"treesitter-unit".select(true)<CR>',
	},
	visual_mode = {
		-- Don't copy the replaced text after pasting in visual mode
		-- ["p"] = '"_dP',
		-- ["r"] = ":lua require('vi-viz').vizPattern()<CR>",
		-- ["c"] = ":lua require('vi-viz').vizChange()<CR>",
		["iu"] = ':lua require"treesitter-unit".select()<CR>',
		["au"] = ':lua require"treesitter-unit".select(true)<CR>',
		["t."] = ":VtrSendLinesToRunner<CR>",
		["<"] = "<gv",
		[">"] = ">gv",
		["J"] = ":m '>+1<CR>gv=gv",
		["K"] = ":m '<-2<CR>gv=gv",
	},
	term_mode = {},
	command_mode = {},
}

local lsp_keymappings = {
	normal_mode = {
		-- lsp
		-- ["gk"] = "<Cmd>lua vim.lsp.buf.hover()<CR>",
		-- ["gl"] = "<Cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>",
		-- ["gD"] = "<Cmd>lua vim.lsp.buf.declaration()<CR>",
		-- ["gd"] = "<Cmd>lua vim.lsp.buf.definition()<CR>",
		-- ["gi"] = "<Cmd>lua vim.lsp.buf.implementation()<CR>",
		-- ["gr"] = "<Cmd>lua vim.lsp.buf.references()<CR>",
		-- ["gm"] = "<Cmd>lua vim.lsp.buf.rename()<CR>",
		-- ["ga"] = "<Cmd>lua vim.lsp.buf.code_actions()<CR>",
		-- ["<C-k>"] = "<Cmd>lua vim.lsp.buf.signature_help()<CR>",
		["[d"] = "<Cmd>lua vim.lsp.diagnostic.goto_prev()<CR>",
		["]d"] = "<Cmd>lua vim.lsp.diagnostic.goto_next()<CR>",
		["[e"] = "<Cmd>Lspsaga diagnostic_jump_next<CR>",
		["]e"] = "<Cmd>Lspsaga diagnostic_jump_prev<CR>",
	},
}

local function set_keymaps(mode, key, val)
	local opt = generic_opts[mode] and generic_opts[mode] or opts
	if type(val) == "table" then
		opt = val[2]
		val = val[1]
	end
	vim.api.nvim_set_keymap(mode, key, val, opt)
end

local function map(mode, keymaps)
	mode = mode_adapters[mode] and mode_adapters[mode] or mode
	for k, v in pairs(keymaps) do
		set_keymaps(mode, k, v)
	end
end

local function setup()
	for mode, mapping in pairs(keymappings) do
		map(mode, mapping)
	end
	for mode, mapping in pairs(lsp_keymappings) do
		map(mode, mapping)
	end
	require("agrp").set({
		-- formatting = {
		-- 	{
		-- 		"BufWritePre",
		-- 		"*",
		-- 		function()
		-- 			vim.cmd([[lua vim.lsp.buf.formatting()]])
		-- 			vim.cmd([[w]])
		-- 		end,
		-- 	},
		-- },
		eslint_fix = {
			{
				"BufWritePre",
				"*.tsx,*.ts",
				function()
					vim.cmd([[TSLspImportAll]])
					vim.cmd([[TSLspOrganize]])
					vim.cmd([[EslintFixAll]])
				end,
			},
		},
		-- shell_fix = {
		-- 	{
		-- 		"BufWritePost",
		-- 		"*.sh",
		-- 		function()
		-- 			vim.cmd([[!shellharden --replace %:p]])
		-- 			vim.cmd([[edit]])
		-- 		end,
		-- 	},
		-- },
		jump_to_the_last_position = {
			{
				"BufReadPost",
				"*",
				function()
					local last_pos = vim.fn.line([['"]])
					if last_pos >= 1 and last_pos <= vim.fn.line("$") then
						vim.cmd([[execute 'normal! g`"']])
					end
				end,
			},
		},
		-- The native implementation of vim-higlihghtedyank in NeoVim
		highlighted_yank = {
			{ "TextYankPost", "*", vim.highlight.on_yank },
		},
	})
end

setup()
