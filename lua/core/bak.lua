local flat = require("core.flat")
local opt = vim.opt
local tab = 2

local options = function()
	-- undo dir
	opt.swapfile = false
	opt.undofile = true
	opt.backup = true
	opt.writebackup = true
	opt.backupdir = flat.cache_dir .. "backup"
	opt.undodir = flat.cache_dir .. "undo"

	opt.fileencoding = "utf-8"
	opt.clipboard = "unnamed,unnamedplus"
	opt.mouse = "a"
	opt.conceallevel = 2

	-- tabs
	opt.expandtab = true
	opt.shiftwidth = tab
	opt.softtabstop = tab
	opt.tabstop = tab

	-- ?
	opt.breakindent = true

	-- searching
	opt.ignorecase = true
	opt.smartcase = true
	opt.inccommand = "split"

	-- display
	opt.cmdheight = 1
	opt.list = true
	-- vim.opt.listchars = {
	-- 	tab = "▓░",
	-- 	trail = "↔",
	-- 	eol = "⏎",
	-- 	extends = "→",
	-- 	precedes = "←",
	-- 	nbsp = "␣",
	-- }
	opt.ruler = false
	opt.showmode = false
	opt.number = true
	opt.numberwidth = 3
	opt.relativenumber = true
	opt.showmatch = true
	opt.showtabline = 2
	opt.completeopt = "menuone,noselect"
	-- vim.opt.fillchars = {
	-- 	diff = "░",
	-- 	eob = "‣",
	-- 	fold = "░",
	-- 	foldopen = "▾",
	-- 	foldsep = "│",
	-- 	foldclose = "▸",
	-- }
	opt.pumblend = 30
	opt.shada = [[!,'1000,<50,s10,h]] -- Store 1000 entries on oldfiles

	-- indents
	vim.opt.breakindent = true
	-- TODO: cannot set formatoptions?
	--vim.opt.formatoptions:append{'n', 'm', 'M', 'j'}
	vim.o.formatoptions = vim.o.formatoptions .. "nmMj"
	vim.opt.formatlistpat = [[^\s*\%(\d\+\|[-a-z]\)\%(\ -\|[]:.)}\t]\)\?\s\+]]
	vim.opt.fixendofline = false
	vim.opt.showbreak = [[→]]
	vim.opt.smartindent = true
end
options()
