vim.g.did_load_filetypes = 1

local groups = {
	-- Apache config file
	{ ".htaccess,*/etc/httpd/*.conf", "setf apache" },
	{ "*.awk", "setf awk" },
	{ "*.c", "setf c" },
	{ "*.cxx,*.c++,*.hh,*.hxx,*.hpp,*.ipp,*.moc,*.tcc,*.inl", "setf cpp" },
	{ "*.h", "setf c" },
	{ "*.css", "setf css" },
	{ "configure.in,configure.ac", "setf config" },
	{ "*.diff,*.rej", "setf diff" },
	{ "Dockerfile,*.Dockerfile", "setf dockerfile" },
	{ "COMMIT_EDITMSG", "setf gitcommit" },
	{ "MERGE_MSG", "setf gitcommit" },
	{ "*.git/config,.gitconfig,.gitmodules", "setf gitconfig" },
	{ "*.git/modules/*/config", "setf gitconfig" },
	{ "*/.config/git/config", "setf gitconfig" },
	{ "*.go", "setf go" },
	{ "*.html,*.htm,*.shtml,*.stm", "setf html" },
	{ "*.js,*.javascript,*.es,*.jsx", "setf javascript" },
	{ "*.jsx", "setf javascriptreact" },
	{ "*.ts", "setf typescript" },
	{ "*.tsx", "setf typescriptreact" },
	{ "*.json,*.jsonp", "setf json" },
	{ "*.less", "setf less" },
	{ "*.lua,*.rockspec", "setf lua" },
	{ "*[mM]akefile,*.mk,*.mak,*.dsp", "setf make" },
	{ "*.man", "setf man" },
	{ "*/etc/man.conf,man.config", "setf manconf" },
	{ "*.markdown,*.mdown,*.mkd,*.mkdn,*.mdwn,*.md", "setf markdown" },
	{ "*.org", "setf org" },
	-- Perl
	{ "*.pl,*.PL,*.pm,*.fcgi", "setf perl" },
	{ "*.plx,*.al", "setf perl" },
	{ "*.p6,*.pm6,*.pl6", "setf perl6" },
	-- Perl POD
	{ "*.pod", "setf pod" },
	{ "*.pod6", "setf pod6" },
	{ "*.py,*.pyw", "setf python" },
	{ [[*.php,*.php\d,*.phtml,*.ctp]], "setf php" },
	{ "robots.txt", "setf robots" },
	{ "*.rs", "setf rust" },
	-- Sass
	{ "*.sass", "setf sass" },
	-- SCSS
	{ "*.scss", "setf scss" },
	-- sed
	{ "*.sed", "setf sed" },
	{ "*/etc/services", "setf services" },
	{ "*.sql", "setf mysql" },
	{ "ssh_config,*/.ssh/config", "setf sshconfig" },
	{ "sshd_config", "setf sshdconfig" },
	{ "*/etc/sysctl.conf,*/etc/sysctl.d/*.conf", "setf sysctl" },
	{ "*/etc/sudoers,sudoers.tmp", "setf sudoers" },
	{ "*.svg", "setf svg" },
	{ "*.scm", "setf lisp" },
	{ "*.stumpwmrc", "setf lisp" },
	{ "*.sh", "setf sh" },
	{ "tags", "setf tags" },
	{ "tags-??", "setf tags" },
	{ "*.latex,*.sty,*.dtx,*.ltx,*.bbl", "setf tex" },
	{ "*.tex", "setf tex" },
	{ "*.xhtml,*.xht", "setf xhtml" },
	{ "*.xml", "setf xml" },
	{ "*.xmi", "setf xml" },
	{ "*.yaml,*.yml", "setf yaml" },
	{ "*.txt,*.text,README", "setf text" },
	{ "*.v", "setf vlang" },
}

require("agrp").set({ filetypedetect = { ["BufNewFile,BufRead"] = groups } })
