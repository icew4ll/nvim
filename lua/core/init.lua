-- INIT.LUA
local core_modules = {
	"core.boot",
	"core.filetypes",
	"core.settings",
	"modules",
	"core.mapping",
	"core.options",
	-- "_plugins",
}

for _, module in ipairs(core_modules) do
	local ok, err = pcall(require, module)
	if not ok then
		vim.notify("Error loading " .. module .. "\n\n" .. err, vim.log.levels.WARN)
		return
	end
end
