local execute = vim.api.nvim_command
local fn = vim.fn
-- ~/.local/share/nvim/site/pack/
local pack_path = fn.stdpath("data") .. "/site/pack"
local fmt = string.format

for _, p in ipairs({
	{ "delphinus/agrp.nvim" },
	{ "delphinus/mappy.nvim" },
	{ "lewis6991/impatient.nvim" },
	{ "wbthomason/packer.nvim", opt = true },
}) do
	local dir = p.opt and "opt" or "start"
	local package = p[1]
	local name = package:match("[^/]+$")
	local path = fmt("%s/packer/%s/%s", pack_path, dir, name)
	local st = vim.loop.fs_stat(path)
	if not st or st.type ~= "directory" then
		execute(fmt("!git clone --depth 1 https://github.com/%s %s", package, path))
	end
end

do
	local ok, _ = pcall(require, "impatient")
	if not ok then
		vim.notify("impatient.nvim not installed, restart nvim", vim.log.levels.WARN)
	end
end

do
	local ok, _ = pcall(require, "packer_compiled")
	if not ok then
		vim.notify("packer_compiled not installed", vim.log.levels.WARN)
	end
end

