-- Automatically generated packer.nvim plugin loader code

if vim.api.nvim_call_function('has', {'nvim-0.5'}) ~= 1 then
  vim.api.nvim_command('echohl WarningMsg | echom "Invalid Neovim version for packer.nvim! | echohl None"')
  return
end

vim.api.nvim_command('packadd packer.nvim')

local no_errors, error_msg = pcall(function()

  local time
  local profile_info
  local should_profile = false
  if should_profile then
    local hrtime = vim.loop.hrtime
    profile_info = {}
    time = function(chunk, start)
      if start then
        profile_info[chunk] = hrtime()
      else
        profile_info[chunk] = (hrtime() - profile_info[chunk]) / 1e6
      end
    end
  else
    time = function(chunk, start) end
  end
  
local function save_profiles(threshold)
  local sorted_times = {}
  for chunk_name, time_taken in pairs(profile_info) do
    sorted_times[#sorted_times + 1] = {chunk_name, time_taken}
  end
  table.sort(sorted_times, function(a, b) return a[2] > b[2] end)
  local results = {}
  for i, elem in ipairs(sorted_times) do
    if not threshold or threshold and elem[2] > threshold then
      results[i] = elem[1] .. ' took ' .. elem[2] .. 'ms'
    end
  end

  _G._packer = _G._packer or {}
  _G._packer.profile_output = results
end

time([[Luarocks path setup]], true)
local package_path_str = "/home/ice/.cache/nvim/packer_hererocks/2.1.0-beta3/share/lua/5.1/?.lua;/home/ice/.cache/nvim/packer_hererocks/2.1.0-beta3/share/lua/5.1/?/init.lua;/home/ice/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/luarocks/rocks-5.1/?.lua;/home/ice/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/luarocks/rocks-5.1/?/init.lua"
local install_cpath_pattern = "/home/ice/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/lua/5.1/?.so"
if not string.find(package.path, package_path_str, 1, true) then
  package.path = package.path .. ';' .. package_path_str
end

if not string.find(package.cpath, install_cpath_pattern, 1, true) then
  package.cpath = package.cpath .. ';' .. install_cpath_pattern
end

time([[Luarocks path setup]], false)
time([[try_loadstring definition]], true)
local function try_loadstring(s, component, name)
  local success, result = pcall(loadstring(s), name, _G.packer_plugins[name])
  if not success then
    vim.schedule(function()
      vim.api.nvim_notify('packer.nvim: Error running ' .. component .. ' for ' .. name .. ': ' .. result, vim.log.levels.ERROR, {})
    end)
  end
  return result
end

time([[try_loadstring definition]], false)
time([[Defining packer_plugins]], true)
_G.packer_plugins = {
  LuaSnip = {
    after = { "cmp_luasnip" },
    config = { "\27LJ\2\n�\1\0\0\5\0\t\0\0206\0\0\0006\2\1\0'\3\2\0B\0\3\3\14\0\0\0X\2\1�K\0\1\0009\2\3\0019\2\4\0025\4\5\0B\2\2\0016\2\1\0'\4\6\0B\2\2\0029\2\a\2B\2\1\0016\2\1\0'\4\b\0B\2\2\1K\0\1\0\18modules.snips\tload luasnip/loaders/from_vscode\1\0\2\fhistory\2\17updateevents\29TextChanged,TextChangedI\15set_config\vconfig\fluasnip\frequire\npcall\0" },
    load_after = {
      ["nvim-cmp"] = true
    },
    loaded = false,
    needs_bufread = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/LuaSnip",
    url = "https://github.com/L3MON4D3/LuaSnip",
    wants = { "friendly-snippets" }
  },
  ["agrp.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/agrp.nvim",
    url = "https://github.com/delphinus/agrp.nvim"
  },
  ["cmp-buffer"] = {
    after = { "cmp-path" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-buffer/after/plugin/cmp_buffer.lua" },
    load_after = {
      ["cmp-nvim-lsp"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-buffer",
    url = "https://github.com/hrsh7th/cmp-buffer"
  },
  ["cmp-cmdline"] = {
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-cmdline/after/plugin/cmp_cmdline.lua" },
    load_after = {
      ["nvim-cmp"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-cmdline",
    url = "https://github.com/hrsh7th/cmp-cmdline"
  },
  ["cmp-emoji"] = {
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-emoji/after/plugin/cmp_emoji.lua" },
    load_after = {
      ["compe-tmux"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-emoji",
    url = "https://github.com/hrsh7th/cmp-emoji"
  },
  ["cmp-nvim-lsp"] = {
    after = { "cmp-buffer" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-nvim-lsp/after/plugin/cmp_nvim_lsp.lua" },
    load_after = {
      ["cmp-nvim-lua"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-nvim-lsp",
    url = "https://github.com/hrsh7th/cmp-nvim-lsp"
  },
  ["cmp-nvim-lua"] = {
    after = { "cmp-nvim-lsp" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-nvim-lua/after/plugin/cmp_nvim_lua.lua" },
    load_after = {
      cmp_luasnip = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-nvim-lua",
    url = "https://github.com/hrsh7th/cmp-nvim-lua"
  },
  ["cmp-path"] = {
    after = { "compe-tmux" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-path/after/plugin/cmp_path.lua" },
    load_after = {
      ["cmp-buffer"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-path",
    url = "https://github.com/hrsh7th/cmp-path"
  },
  cmp_luasnip = {
    after = { "cmp-nvim-lua" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp_luasnip/after/plugin/cmp_luasnip.lua" },
    load_after = {
      LuaSnip = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp_luasnip",
    url = "https://github.com/saadparwaiz1/cmp_luasnip"
  },
  ["cokeline.nvim"] = {
    config = { "\27LJ\2\ne\0\1\5\1\5\0\0149\1\0\0\15\0\1\0X\2\6�-\1\0\0'\3\1\0'\4\2\0B\1\3\2\14\0\1\0X\2\4�-\1\0\0'\3\3\0'\4\4\0B\1\3\2L\1\2\0\0�\afg\vNormal\abg\16ColorColumn\15is_focusede\0\1\5\1\5\0\0149\1\0\0\15\0\1\0X\2\6�-\1\0\0'\3\1\0'\4\2\0B\1\3\2\14\0\1\0X\2\4�-\1\0\0'\3\3\0'\4\4\0B\1\3\2L\1\2\0\0�\abg\16ColorColumn\afg\vNormal\15is_focused*\0\1\3\0\3\0\5'\1\0\0009\2\1\0009\2\2\2&\1\2\1L\1\2\0\ticon\fdevicon\6 !\0\1\2\0\2\0\0039\1\0\0009\1\1\1L\1\2\0\ncolor\fdevicon\29\0\1\2\0\1\0\0029\1\0\0L\1\2\0\18unique_prefix\"\0\1\3\0\2\0\0049\1\0\0'\2\1\0&\1\2\1L\1\2\0\6 \rfilename�\2\1\0\n\0\24\0+6\0\0\0'\2\1\0B\0\2\0029\0\2\0006\1\0\0'\3\3\0B\1\2\0029\1\4\0015\3\n\0005\4\6\0003\5\5\0=\5\a\0043\5\b\0=\5\t\4=\4\v\0034\4\6\0005\5\r\0003\6\f\0=\6\14\0053\6\15\0=\6\a\5>\5\1\0045\5\17\0003\6\16\0=\6\14\5\18\6\0\0'\b\18\0'\t\a\0B\6\3\2=\6\a\5>\5\2\0045\5\20\0003\6\19\0=\6\14\5>\5\3\0045\5\21\0>\5\4\0045\5\22\0>\5\5\4=\4\23\3B\1\2\0012\0\0�K\0\1\0\15components\1\0\1\ttext\6 \1\0\2\ttext\b delete_buffer_on_left_click\2\1\0\0\0\fComment\1\0\1\nstyle\vitalic\0\0\ttext\1\0\0\0\15default_hl\1\0\0\abg\0\afg\1\0\0\0\nsetup\rcokeline\fget_hex\19cokeline/utils\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cokeline.nvim",
    url = "https://github.com/noib3/cokeline.nvim"
  },
  ["compe-tmux"] = {
    after = { "cmp-emoji" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/compe-tmux/after/plugin/cmp_tmux.vim" },
    load_after = {
      ["cmp-path"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/compe-tmux",
    url = "https://github.com/andersevenrud/compe-tmux"
  },
  ["elvish.vim"] = {
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/elvish.vim",
    url = "https://github.com/dmix/elvish.vim"
  },
  ["fm-nvim"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/fm-nvim",
    url = "https://github.com/is0n/fm-nvim"
  },
  ["friendly-snippets"] = {
    after = { "nvim-cmp" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/friendly-snippets",
    url = "https://github.com/rafamadriz/friendly-snippets"
  },
  ["gitsigns.nvim"] = {
    config = { "\27LJ\2\n�\3\0\0\5\0\18\0\0216\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\14\0005\3\4\0005\4\3\0=\4\5\0035\4\6\0=\4\a\0035\4\b\0=\4\t\0035\4\n\0=\4\v\0035\4\f\0=\4\r\3=\3\15\0025\3\16\0=\3\17\2B\0\2\1K\0\1\0\28current_line_blame_opts\1\0\1\ndelay\3\n\nsigns\1\0\3\nnumhl\2\23current_line_blame\2\14word_diff\1\17changedelete\1\0\2\ttext\t🐝\ahl\19GitSignsChange\14topdelete\1\0\2\ttext\t🪲\ahl\19GitSignsDelete\vdelete\1\0\2\ttext\t🪲\ahl\19GitSignsDelete\vchange\1\0\2\ttext\t🤎\ahl\19GitSignsChange\badd\1\0\0\1\0\2\ttext\t🌲\ahl\16GitSignsAdd\nsetup\rgitsigns\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/gitsigns.nvim",
    url = "https://github.com/lewis6991/gitsigns.nvim"
  },
  ["gruvbox-flat.nvim"] = {
    config = { "\27LJ\2\nF\0\0\3\0\4\0\0066\0\0\0009\0\1\0009\0\2\0'\2\3\0B\0\2\1K\0\1\0\22colo gruvbox-flat\17nvim_command\bapi\bvim\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/gruvbox-flat.nvim",
    url = "https://github.com/eddyekofo94/gruvbox-flat.nvim"
  },
  ["impatient.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/impatient.nvim",
    url = "https://github.com/lewis6991/impatient.nvim"
  },
  ["indent-blankline.nvim"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/indent-blankline.nvim",
    url = "https://github.com/lukas-reineke/indent-blankline.nvim"
  },
  ["ion-vim"] = {
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/ion-vim",
    url = "https://github.com/vmchale/ion-vim"
  },
  ["lightspeed.nvim"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/lightspeed.nvim",
    url = "https://github.com/ggandor/lightspeed.nvim"
  },
  ["lsp_signature.nvim"] = {
    config = { "\27LJ\2\n�\2\0\0\6\0\a\0\f6\0\0\0006\2\1\0'\3\2\0B\0\3\3\15\0\0\0X\2\5�9\2\3\0015\4\4\0005\5\5\0=\5\6\4B\2\2\1K\0\1\0\17handler_opts\1\0\1\vborder\vsingle\1\0\f\14max_width\3x\15max_height\3\22\fpadding\5\ffix_pos\2\tbind\2\14doc_lines\3\0\17hi_parameter\vSearch\16hint_prefix\t \16hint_enable\2\20floating_window\2\vzindex\3�\1\16hint_scheme\vString\nsetup\18lsp_signature\frequire\npcall\0" },
    load_after = {
      ["nvim-lspconfig"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/lsp_signature.nvim",
    url = "https://github.com/ray-x/lsp_signature.nvim"
  },
  ["lspkind-nvim"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/lspkind-nvim",
    url = "https://github.com/onsails/lspkind-nvim"
  },
  ["lualine.nvim"] = {
    config = { "\27LJ\2\n\\\0\0\5\0\5\1\0156\0\0\0009\0\1\0009\0\2\0006\2\0\0009\2\1\0029\2\3\2'\4\4\0B\2\2\0A\0\0\2\t\0\0\0X\0\2�+\0\1\0X\1\1�+\0\2\0L\0\2\0\b%:t\vexpand\nempty\afn\bvim\2G\0\0\3\0\3\0\f6\0\0\0009\0\1\0009\0\2\0)\2\0\0B\0\2\2)\1P\0\0\1\0\0X\0\2�+\0\1\0X\1\1�+\0\2\0L\0\2\0\rwinwidth\afn\bvim�\1\0\0\6\0\a\0\0276\0\0\0009\0\1\0009\0\2\0'\2\3\0B\0\2\0026\1\0\0009\1\1\0019\1\4\1'\3\5\0\18\4\0\0'\5\6\0&\4\5\4B\1\3\2\r\2\1\0X\2\v�\21\2\1\0)\3\0\0\1\3\2\0X\2\4�\21\2\1\0\21\3\0\0\0\2\3\0X\2\2�+\2\1\0X\3\1�+\2\2\0L\2\2\0\6;\t.git\ffinddir\n%:p:h\vexpand\afn\bvimI\0\1\5\1\4\0\b6\1\0\0009\1\1\1-\3\0\0009\3\2\0039\3\3\3\18\4\0\0B\1\3\1K\0\1\0\3�\14lualine_c\rsections\vinsert\ntableI\0\1\5\1\4\0\b6\1\0\0009\1\1\1-\3\0\0009\3\2\0039\3\3\3\18\4\0\0B\1\3\1K\0\1\0\3�\14lualine_x\rsections\vinsert\ntable�\3\0\0\a\1&\0M5\0\1\0-\1\0\0009\1\0\1=\1\2\0-\1\0\0009\1\3\1=\1\4\0-\1\0\0009\1\5\1=\1\6\0-\1\0\0009\1\5\1=\1\a\0-\1\0\0009\1\5\1=\1\b\0-\1\0\0009\1\t\1=\1\n\0-\1\0\0009\1\0\1=\1\v\0-\1\0\0009\1\f\1=\1\r\0-\1\0\0009\1\f\1=\1\14\0-\1\0\0009\1\f\1=\1\15\0-\1\0\0009\1\16\1=\1\17\0-\1\0\0009\1\18\1=\1\19\0-\1\0\0009\1\18\1=\1\20\0-\1\0\0009\1\0\1=\1\21\0-\1\0\0009\1\0\1=\1\22\0-\1\0\0009\1\23\1=\1\24\0-\1\0\0009\1\23\1=\1\25\0-\1\0\0009\1\23\1=\1\26\0-\1\0\0009\1\0\1=\1\27\0-\1\0\0009\1\0\1=\1\28\0006\1\29\0009\1\30\0019\1\31\1'\3 \0006\4\29\0009\4!\0049\4\"\4B\4\1\0028\4\4\0'\5#\0-\6\0\0009\6$\6&\3\6\3B\1\2\1'\1%\0L\1\2\0\1�\t🌙\abg\f guibg=\tmode\afn\27hi! LualineMode guifg=\17nvim_command\bapi\bvim\6t\6!\ar?\arm\6r\tcyan\ace\acv\aRv\6R\vviolet\aic\vyellow\6\19\6S\6s\vorange\ano\6c\fmagenta\6V\6\22\6v\tblue\6i\ngreen\6n\1\0\0\bred�\2\0\0\r\0\14\1&'\0\0\0006\1\1\0009\1\2\0019\1\3\1)\3\0\0'\4\4\0B\1\3\0026\2\1\0009\2\5\0029\2\6\2B\2\1\0026\3\a\0\18\5\2\0B\3\2\2\v\3\0\0X\3\1�L\0\2\0006\3\b\0\18\5\2\0B\3\2\4X\6\14�9\b\t\a9\b\n\b\15\0\b\0X\t\n�6\t\1\0009\t\v\t9\t\f\t\18\v\b\0\18\f\1\0B\t\3\2\b\t\0\0X\t\2�9\t\r\aL\t\2\0E\6\3\3R\6�L\0\2\0\tname\nindex\afn\14filetypes\vconfig\vipairs\tnext\23get_active_clients\blsp\rfiletype\24nvim_buf_get_option\bapi\bvim\vNo Lsp����\31�\r\1\0\f\0Z\0�\0016\0\0\0'\2\1\0B\0\2\0025\1\2\0005\2\4\0003\3\3\0=\3\5\0023\3\6\0=\3\a\0023\3\b\0=\3\t\0025\3\22\0005\4\n\0005\5\16\0005\6\14\0005\a\f\0009\b\v\1=\b\v\a9\b\r\1=\b\r\a=\a\15\6=\6\17\0055\6\19\0005\a\18\0009\b\v\1=\b\v\a9\b\r\1=\b\r\a=\a\15\6=\6\20\5=\5\21\4=\4\23\0035\4\24\0004\5\0\0=\5\25\0044\5\0\0=\5\26\0044\5\0\0=\5\27\0044\5\0\0=\5\28\0044\5\0\0=\5\29\0044\5\0\0=\5\30\4=\4\31\0035\4 \0004\5\0\0=\5\25\0044\5\0\0=\5!\0044\5\0\0=\5\27\0044\5\0\0=\5\28\0044\5\0\0=\5\29\0044\5\0\0=\5\30\4=\4\"\0033\4#\0003\5$\0\18\6\4\0005\b&\0003\t%\0>\t\1\b5\t'\0=\t(\bB\6\2\1\18\6\4\0005\b)\0009\t\5\2=\t*\bB\6\2\1\18\6\4\0005\b+\0009\t\5\2=\t*\b5\t-\0009\n,\1=\n\v\t=\t.\bB\6\2\1\18\6\4\0005\b/\0005\t0\0=\t1\b5\t2\0=\t3\b5\t6\0005\n5\0009\v4\1=\v\v\n=\n7\t5\n9\0009\v8\1=\v\v\n=\n:\t5\n<\0009\v;\1=\v\v\n=\n=\t=\t>\bB\6\2\1\18\6\4\0005\b@\0003\t?\0>\t\1\b5\tA\0=\t.\bB\6\2\1\18\6\5\0005\bB\0006\tC\0009\tD\t=\tE\b9\t\a\2=\t*\b5\tG\0009\nF\1=\n\v\t=\t.\bB\6\2\1\18\6\5\0005\bH\0006\tC\0009\tD\t=\tE\b5\tI\0009\nF\1=\n\v\t=\t.\bB\6\2\1\18\6\5\0005\bJ\0005\tL\0009\nK\1=\n\v\t=\t.\bB\6\2\1\18\6\5\0005\bM\0005\tN\0=\t3\b5\tP\0005\nO\0009\vF\1=\v\v\n=\nQ\t5\nS\0009\vR\1=\v\v\n=\nT\t5\nU\0009\v4\1=\v\v\n=\nV\t=\tW\b9\t\a\2=\t*\bB\6\2\1\18\6\5\0005\bX\0B\6\2\0019\6Y\0\18\b\3\0B\6\2\0012\0\0�K\0\1\0\nsetup\1\2\0\0\rlocation\15diff_color\fremoved\1\0\0\rmodified\1\0\0\vorange\nadded\1\0\0\1\0\0\1\0\3\rmodified\t柳 \fremoved\t \nadded\t \1\2\0\0\tdiff\1\0\1\bgui\tbold\vviolet\1\2\1\0\vbranch\ticon\b\1\0\1\bgui\tbold\1\2\1\0\15fileformat\18icons_enabled\1\1\0\1\bgui\tbold\ngreen\bfmt\nupper\vstring\1\2\0\0\15o:encoding\1\0\2\bgui\tbold\afg\f#ffffff\1\0\1\ticon\t📖\0\22diagnostics_color\15color_info\1\0\0\tcyan\15color_warn\1\0\0\vyellow\16color_error\1\0\0\1\0\0\bred\fsymbols\1\0\3\twarn\t \nerror\t \tinfo\t \fsources\1\2\0\0\20nvim_diagnostic\1\2\0\0\16diagnostics\ncolor\1\0\1\bgui\tbold\fmagenta\1\2\0\0\rfilename\tcond\1\2\0\0\rfilesize\fpadding\1\0\1\nright\3\1\1\0\1\ncolor\16LualineMode\0\0\0\22inactive_sections\14lualine_v\1\0\0\rsections\14lualine_x\14lualine_c\14lualine_z\14lualine_y\14lualine_b\14lualine_a\1\0\0\foptions\1\0\0\ntheme\rinactive\1\0\0\1\0\0\vnormal\1\0\0\6c\1\0\0\abg\1\0\0\afg\1\0\2\25component_separators\5\23section_separators\5\24check_git_workspace\0\18hide_in_width\0\21buffer_not_empty\1\0\0\0\1\0\v\bred\f#ec5f67\fmagenta\f#c678dd\abg\f#192330\vorange\f#FF8800\ngreen\f#98be65\tblue\f#51afef\vviolet\f#a9a1e1\rdarkblue\f#081633\tcyan\f#008080\vyellow\f#ECBE7B\afg\f#bbc2cf\flualine\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/lualine.nvim",
    url = "https://github.com/nvim-lualine/lualine.nvim"
  },
  ["mappy.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/mappy.nvim",
    url = "https://github.com/delphinus/mappy.nvim"
  },
  ["null-ls.nvim"] = {
    config = { "\27LJ\2\n�\5\0\0\t\0#\2V6\0\0\0'\2\1\0B\0\2\0024\1\r\0009\2\2\0009\2\3\0029\2\4\2>\2\1\0019\2\2\0009\2\3\0029\2\5\2>\2\2\0019\2\2\0009\2\3\0029\2\6\2>\2\3\0019\2\2\0009\2\3\0029\2\a\2>\2\4\0019\2\2\0009\2\b\0029\2\t\2>\2\5\0019\2\2\0009\2\3\0029\2\n\2>\2\6\0019\2\2\0009\2\3\0029\2\v\2>\2\a\0019\2\2\0009\2\b\0029\2\f\2>\2\b\0019\2\2\0009\2\3\0029\2\r\2>\2\t\0019\2\2\0009\2\3\0029\2\14\0029\2\15\0025\4\17\0005\5\16\0=\5\18\4B\2\2\2>\2\n\0019\2\2\0009\2\b\0029\2\19\0029\2\15\0025\4\21\0005\5\20\0=\5\18\0045\5\22\0006\6\23\0009\6\24\0069\6\25\6'\b\26\0B\6\2\0?\6\0\0=\5\27\4B\2\2\2>\2\v\0019\2\2\0009\2\b\0029\2\28\0029\2\15\0025\4\31\0005\5\29\0006\6\23\0009\6\24\0069\6\25\6'\b\30\0B\6\2\0?\6\0\0=\5\27\4B\2\2\0?\2\1\0009\2 \0005\4!\0=\1\"\4B\2\2\1K\0\1\0\fsources\1\0\0\nsetup\1\0\0#~/.config/null-ls/yamllint.yml\1\2\0\0\18--config-file\ryamllint\15extra_args\"~/.config/null-ls/selene.toml\vexpand\afn\bvim\1\2\0\0\r--config\1\0\0\1\2\0\0\blua\vselene\14filetypes\1\0\0\1\t\0\0\thtml\tjson\tyaml\rmarkdown\fgraphql\bcss\tscss\tless\twith\14prettierd\16shellharden\15shellcheck\nshfmt\frustfmt\vpylint\16diagnostics\27reorder_python_imports\nblack\vstylua\nguile\15formatting\rbuiltins\fnull-ls\frequire\5����\4\25����\4\0" },
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/null-ls.nvim",
    url = "https://github.com/jose-elias-alvarez/null-ls.nvim"
  },
  ["nvim-autopairs"] = {
    config = { "\27LJ\2\n�\2\0\0\n\0\15\0\0256\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\2B\0\2\0016\0\0\0'\2\6\0B\0\2\0026\1\0\0'\3\a\0B\1\2\0029\2\b\1\18\4\2\0009\2\t\2'\5\n\0009\6\v\0005\b\r\0005\t\f\0=\t\14\bB\6\2\0A\2\2\1K\0\1\0\rmap_char\1\0\0\1\0\1\btex\5\20on_confirm_done\17confirm_done\aon\nevent\bcmp\"nvim-autopairs.completion.cmp\21disable_filetype\1\0\0\1\3\0\0\20TelescopePrompt\bvim\nsetup\19nvim-autopairs\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-autopairs",
    url = "https://github.com/windwp/nvim-autopairs"
  },
  ["nvim-cmp"] = {
    after = { "LuaSnip", "cmp-cmdline" },
    config = { "\27LJ\2\nC\0\1\4\0\4\0\a6\1\0\0'\3\1\0B\1\2\0029\1\2\0019\3\3\0B\1\2\1K\0\1\0\tbody\15lsp_expand\fluasnip\frequire�\1\0\2\b\0\v\0\0186\2\1\0009\2\2\2'\4\3\0006\5\4\0'\a\5\0B\5\2\0029\5\6\0059\6\0\0018\5\6\0059\6\0\1B\2\4\2=\2\0\0015\2\b\0009\3\t\0009\3\n\0038\2\3\2=\2\a\1L\1\2\0\tname\vsource\1\0\a\tpath\t🧭\rnvim_lua\t🌙\rnvim_lsp\t🏹\fluasnip\t📜\ttmux\t💻\vbuffer\t🔮\nemoji\t🍵\tmenu\nicons\20modules.lspkind\frequire\n%s %s\vformat\vstring\tkind�\2\0\1\t\1\f\0!-\1\0\0009\1\0\1B\1\1\2\15\0\1\0X\2\4�-\1\0\0009\1\1\1B\1\1\1X\1\23�6\1\2\0'\3\3\0B\1\2\0029\1\4\1B\1\1\2\15\0\1\0X\2\14�6\1\5\0009\1\6\0019\1\a\0016\3\5\0009\3\b\0039\3\t\3'\5\n\0+\6\2\0+\a\2\0+\b\2\0B\3\5\2'\4\v\0B\1\3\1X\1\2�\18\1\0\0B\1\1\1K\0\1\0\1�\5!<Plug>luasnip-expand-or-jump\27nvim_replace_termcodes\bapi\rfeedkeys\afn\bvim\23expand_or_jumpable\fluasnip\frequire\21select_next_item\fvisible�\2\0\1\t\1\f\0\"-\1\0\0009\1\0\1B\1\1\2\15\0\1\0X\2\4�-\1\0\0009\1\1\1B\1\1\1X\1\24�6\1\2\0'\3\3\0B\1\2\0029\1\4\1)\3��B\1\2\2\15\0\1\0X\2\14�6\1\5\0009\1\6\0019\1\a\0016\3\5\0009\3\b\0039\3\t\3'\5\n\0+\6\2\0+\a\2\0+\b\2\0B\3\5\2'\4\v\0B\1\3\1X\1\2�\18\1\0\0B\1\1\1K\0\1\0\1�\5\28<Plug>luasnip-jump-prev\27nvim_replace_termcodes\bapi\rfeedkeys\afn\bvim\rjumpable\fluasnip\frequire\21select_prev_item\fvisible�\6\1\0\n\0002\0U6\0\0\0006\2\1\0'\3\2\0B\0\3\3\14\0\0\0X\2\1�2\0M�6\2\3\0009\2\4\2'\3\6\0=\3\5\0029\2\a\0015\4\v\0005\5\t\0003\6\b\0=\6\n\5=\5\f\0045\5\14\0003\6\r\0=\6\15\5=\5\16\0045\5\19\0009\6\17\0019\6\18\6B\6\1\2=\6\20\0059\6\17\0019\6\21\6B\6\1\2=\6\22\0059\6\17\0019\6\23\6)\b��B\6\2\2=\6\24\0059\6\17\0019\6\23\6)\b\4\0B\6\2\2=\6\25\0059\6\17\0019\6\26\6B\6\1\2=\6\27\0059\6\17\0019\6\28\6B\6\1\2=\6\29\0059\6\17\0019\6\30\0065\b!\0009\t\31\0019\t \t=\t\"\bB\6\2\2=\6#\0053\6$\0=\6%\0053\6&\0=\6'\5=\5\17\0044\5\n\0005\6(\0>\6\1\0055\6)\0>\6\2\0055\6*\0>\6\3\0055\6+\0>\6\4\0055\6,\0>\6\5\0055\6-\0>\6\6\0055\6.\0>\6\a\0055\6/\0>\6\b\0055\0060\0>\6\t\5=\0051\4B\2\2\0012\0\0�K\0\1\0K\0\1\0\fsources\1\0\1\tname\forgmode\1\0\1\tname\nemoji\1\0\1\tname\ttmux\1\0\1\tname\tpath\1\0\1\tname\rnvim_lua\1\0\1\tname\vbuffer\1\0\1\tname\fluasnip\1\0\1\tname\28nvim_lsp_signature_help\1\0\1\tname\rnvim_lsp\f<S-Tab>\0\n<Tab>\0\t<CR>\rbehavior\1\0\1\vselect\2\fReplace\20ConfirmBehavior\fconfirm\n<C-e>\nclose\14<C-Space>\rcomplete\n<C-f>\n<C-d>\16scroll_docs\n<C-n>\21select_next_item\n<C-p>\1\0\0\21select_prev_item\fmapping\15formatting\vformat\1\0\0\0\fsnippet\1\0\0\vexpand\1\0\0\0\nsetup\21menuone,noselect\16completeopt\bopt\bvim\bcmp\frequire\npcall\0" },
    load_after = {
      ["friendly-snippets"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-cmp",
    url = "https://github.com/hrsh7th/nvim-cmp"
  },
  ["nvim-colorizer.lua"] = {
    config = { "\27LJ\2\nA\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0B\0\2\1K\0\1\0\1\2\0\0\6*\nsetup\14colorizer\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-colorizer.lua",
    url = "https://github.com/norcalli/nvim-colorizer.lua"
  },
  ["nvim-comment"] = {
    config = { "\27LJ\2\n�\2\0\0\4\0\n\0(6\0\0\0009\0\1\0009\0\2\0)\2\0\0'\3\3\0B\0\3\2\a\0\4\0X\0\5�6\0\5\0'\2\6\0B\0\2\0029\0\a\0B\0\1\0016\0\0\0009\0\1\0009\0\2\0)\2\0\0'\3\3\0B\0\3\2\a\0\b\0X\0\5�6\0\5\0'\2\6\0B\0\2\0029\0\a\0B\0\1\0016\0\0\0009\0\1\0009\0\2\0)\2\0\0'\3\3\0B\0\3\2\a\0\t\0X\0\5�6\0\5\0'\2\6\0B\0\2\0029\0\a\0B\0\1\1K\0\1\0\20javascriptreact\20typescriptreact\25update_commentstring&ts_context_commentstring.internal\frequire\bvue\rfiletype\24nvim_buf_get_option\bapi\bvimO\1\0\4\0\6\0\t6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0003\3\3\0=\3\5\2B\0\2\1K\0\1\0\thook\1\0\0\0\nsetup\17nvim_comment\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-comment",
    url = "https://github.com/terrortylor/nvim-comment"
  },
  ["nvim-lsp-ts-utils"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/nvim-lsp-ts-utils",
    url = "https://github.com/jose-elias-alvarez/nvim-lsp-ts-utils"
  },
  ["nvim-lspconfig"] = {
    after = { "lsp_signature.nvim" },
    config = { "\27LJ\2\nU\0\0\6\0\5\0\n6\0\0\0006\2\1\0009\2\2\0026\4\1\0009\4\3\0049\4\4\4B\4\1\0A\2\0\0A\0\0\1K\0\1\0\20buf_get_clients\blsp\finspect\bvim\nprintk\0\0\4\0\6\0\r6\0\0\0009\0\1\0009\0\2\0006\2\0\0009\2\1\0029\2\3\2B\2\1\0A\0\0\0016\0\0\0009\0\4\0'\2\5\0B\0\2\1K\0\1\0\tedit\bcmd\23get_active_clients\16stop_client\blsp\bvimj\0\0\5\1\b\0\v6\0\0\0009\0\1\0009\0\2\0009\0\3\0005\2\6\0005\3\4\0-\4\0\0=\4\5\3=\3\a\2B\0\2\1K\0\1\0\1\0\15popup_opts\1\0\0\vborder\1\0\0\14goto_next\15diagnostic\blsp\bvimj\0\0\5\1\b\0\v6\0\0\0009\0\1\0009\0\2\0009\0\3\0005\2\6\0005\3\4\0-\4\0\0=\4\5\3=\3\a\2B\0\2\1K\0\1\0\1\0\15popup_opts\1\0\0\vborder\1\0\0\14goto_prev\15diagnostic\blsp\bvim�\1\0\0\2\0\a\0\0246\0\0\0009\0\1\0009\0\2\0\15\0\0\0X\1\6�6\0\0\0009\0\3\0009\0\4\0009\0\5\0B\0\1\1X\0\5�6\0\0\0009\0\3\0009\0\4\0009\0\6\0B\0\1\0016\0\0\0009\0\1\0006\1\0\0009\1\1\0019\1\2\1\19\1\1\0=\1\2\0K\0\1\0\fdisable\venable\15diagnostic\blsp\29lsp_diagnostics_disabled\6b\bvim5\0\0\2\0\3\0\0056\0\0\0009\0\1\0009\0\2\0B\0\1\1K\0\1\0\15open_float\15diagnostic\bvimP\0\0\3\0\6\0\n6\0\0\0009\0\1\0'\2\2\0B\0\2\0016\0\0\0009\0\3\0009\0\4\0009\0\5\0B\0\1\1K\0\1\0\15definition\bbuf\blsp\nsplit\bcmd\bvim�\4\1\0\5\3\"\0_-\0\0\0009\0\0\0'\2\1\0005\3\2\0003\4\3\0B\0\4\1-\0\0\0009\0\0\0'\2\1\0005\3\4\0003\4\5\0B\0\4\1-\0\0\0009\0\6\0'\2\a\0003\3\b\0B\0\3\1-\0\0\0009\0\6\0'\2\t\0003\3\n\0B\0\3\1-\0\2\0\14\0\0\0X\0E�-\0\0\0009\0\6\0'\2\v\0006\3\f\0009\3\r\0039\3\14\0039\3\15\3B\0\3\1-\0\0\0009\0\6\0'\2\16\0006\3\f\0009\3\r\0039\3\14\0039\3\17\3B\0\3\0016\0\f\0009\0\18\0009\0\19\0\18\2\0\0009\0\20\0B\0\2\2\6\0\21\0X\0\r�-\0\0\0009\0\6\0'\2\22\0006\3\f\0009\3\r\0039\3\14\0039\3\23\3B\0\3\1-\0\0\0009\0\6\0'\2\24\0003\3\25\0B\0\3\1-\0\0\0009\0\6\0'\2\26\0006\3\f\0009\3\r\0039\3\14\0039\3\27\3B\0\3\1-\0\0\0009\0\6\0'\2\28\0006\3\f\0009\3\r\0039\3\14\0039\3\29\3B\0\3\1-\0\0\0009\0\6\0'\2\30\0006\3\f\0009\3\r\0039\3\14\0039\3\31\3B\0\3\1-\0\0\0009\0\6\0'\2 \0006\3\f\0009\3\r\0039\3\14\0039\3!\3B\0\3\1K\0\1\0\0\0\1\0\2\0\15references\agf\16declaration\age\vrename\agr\16code_action\aga\0\15<C-w><C-]>\15definition\n<C-]>\thelp\bget\rfiletype\bopt\20type_definition\agd\nhover\bbuf\blsp\bvim\agk\0\agl\0\r<Space>E\rnnoremap\0\1\3\0\0\n<A-K>\14<A-S->\0\1\3\0\0\n<A-J>\r<A-S-Ô>\6n\tbind�\2\1\2\b\3\r\0\0276\2\0\0'\4\1\0\18\6\4\0009\4\2\4\18\a\1\0B\4\3\0A\2\0\0019\2\3\0009\2\4\2\15\0\2\0X\3\4�9\2\3\0009\2\4\2+\3\2\0=\3\5\0026\2\6\0009\2\a\0029\2\b\2\18\4\1\0'\5\t\0'\6\n\0B\2\4\1-\2\0\0009\2\v\0023\4\f\0B\2\2\1K\0\1\0\0\0\1\0\0�\0\20add_buffer_maps\27v:lua.vim.lsp.omnifunc\romnifunc\24nvim_buf_set_option\bapi\bvim\27allow_incremental_sync\nflags\vconfig\vformat\28LSP started: bufnr = %d\nprint\24\1\1\2\2\1\0\0033\1\0\0002\0\0�L\1\2\0\0�\4�\0n\0\1\a\1\5\0\18-\1\0\0009\1\0\0019\1\1\1\18\3\0\0B\1\2\2\15\0\1\0X\2\n�-\1\0\0009\1\0\0019\1\2\1-\3\0\0009\3\0\0039\3\3\3\18\5\0\0'\6\4\0B\3\3\0A\1\0\2L\1\2\0\a�\t.git\tjoin\vexists\vis_dir\tpath�\1\0\1\v\0\6\0\21\18\3\0\0009\1\0\0'\4\1\0'\5\2\0B\1\4\0026\2\3\0005\4\4\0B\2\2\4X\5\b�\18\t\1\0009\a\5\1\18\n\6\0B\a\3\2\15\0\a\0X\b\2�+\a\2\0L\a\2\0E\5\3\3R\5�+\2\1\0L\2\2\0\nmatch\1\b\0\0\n^deno\t^ddc\16^cmp%-look$\16^neco%-vim$\17^git%-vines$\f^murus$\16^skkeleton$\vipairs\5\b.*/\tgsub�\3\0\2\a\0\n\0\0176\2\0\0'\4\1\0B\2\2\0029\3\2\0025\5\3\0005\6\4\0=\6\5\0054\6\0\0=\6\6\0054\6\0\0=\6\a\5B\3\2\0019\3\b\2\18\5\0\0B\3\2\0015\3\t\0K\0\1\0\1\0\1\vsilent\2\17setup_client#filter_out_diagnostics_by_code'filter_out_diagnostics_by_severity\26import_all_priorities\1\0\4\14same_file\3\1\19buffer_content\3\3\fbuffers\3\4\16local_files\3\2\1\0\n enable_import_on_completion\1\29import_all_select_source\1\27update_imports_on_move\1\28import_all_scan_buffers\3d\21disable_commands\1!require_confirmation_on_move\1\21auto_inlay_hints\2\26inlay_hints_highlight\fComment\23import_all_timeout\3�'\ndebug\1\nsetup\22nvim-lsp-ts-utils\frequire3\0\1\4\2\0\0\n-\1\0\0\18\3\0\0B\1\2\2\15\0\1\0X\2\4�-\1\1\0\18\3\0\0B\1\2\2\19\1\1\0L\1\2\0\1\0\2\0007\1\1\5\3\2\0\6-\1\0\0009\1\0\1\18\3\0\0003\4\1\0002\0\0�D\1\3\0\a�\b�\t�\0\21search_ancestors�\5\0\0\v\1)\0A6\0\0\0009\0\1\0009\0\2\0B\0\1\2'\1\3\0&\0\1\0'\1\4\0\18\3\1\0009\1\5\1\18\4\0\0006\5\0\0009\5\1\0059\5\6\5B\5\1\0029\5\a\5\a\5\b\0X\5\2�'\5\t\0X\6\1�'\5\n\0B\1\4\0025\2\v\0-\3\0\0B\3\1\2=\3\f\0025\3\r\0>\1\1\3\18\4\0\0'\5\14\0&\4\5\4>\4\3\3=\3\15\0025\3&\0005\4\21\0005\5\16\0006\6\0\0009\6\17\0066\b\18\0009\b\19\b'\t\20\0B\6\3\2=\6\19\5=\5\22\0045\5\23\0=\5\24\0045\5\25\0005\6\26\0=\6\27\5=\5\28\0045\5\"\0005\6 \0006\a\0\0009\a\29\a9\a\30\a'\t\31\0+\n\2\0B\a\3\2=\a!\6=\6!\5=\5#\0045\5$\0=\5%\4=\4'\3=\3(\2L\2\2\0\5�\rsettings\bLua\1\0\0\14telemetry\1\0\1\venable\1\14workspace\1\0\0\flibrary\1\0\1\20checkThirdParty\1\5\26nvim_get_runtime_file\bapi\16diagnostics\fglobals\1\b\0\0\bvim\rdescribe\ait\16before_each\15after_each\19packer_plugins\ahs\1\0\1\venable\2\15completion\1\0\1\19keywordSnippet\fDisable\fruntime\1\0\0\6;\tpath\fpackage\nsplit\1\0\1\fversion\vLuaJIT\bcmd\14/main.lua\1\3\0\0\0\a-E\14on_attach\1\0\0\nLinux\nmacOS\vDarwin\fsysname\ros_uname\vformat\"%s/bin/%s/lua-language-server#/bin/build/lua-language-server\15os_homedir\tloop\bvim�\14\1\0\18\0N\0�\0016\0\0\0'\2\1\0B\0\2\0026\1\2\0009\1\3\0019\1\4\1B\1\1\2'\2\5\0&\1\2\1\18\2\1\0'\3\6\0&\2\3\0026\3\2\0009\3\a\0039\3\b\3\18\4\3\0'\6\t\0005\a\n\0B\4\3\1\18\4\3\0'\6\v\0005\a\f\0B\4\3\1\18\4\3\0'\6\r\0005\a\14\0B\4\3\1\18\4\3\0'\6\15\0005\a\16\0B\4\3\0016\4\17\0003\5\19\0=\5\18\0046\4\17\0003\5\21\0=\5\20\0046\4\2\0009\4\22\0049\4\23\4'\6\24\0+\a\1\0B\4\3\0014\4\t\0005\5\25\0>\5\1\0045\5\26\0>\5\2\0045\5\27\0>\5\3\0045\5\28\0>\5\4\0045\5\29\0>\5\5\0045\5\30\0>\5\6\0045\5\31\0>\5\a\0045\5 \0>\5\b\0043\5!\0006\6\2\0009\6\"\0069\6#\0066\a\2\0009\a\"\a9\a%\a6\t\2\0009\t\"\t9\t&\t9\t'\t5\n(\0B\a\3\2=\a$\0066\6\2\0009\6\"\0069\6#\0066\a\2\0009\a\"\a9\a%\a6\t\2\0009\t\"\t9\t#\t9\t*\t5\n+\0=\4,\nB\a\3\2=\a)\0066\6\2\0009\6\"\0069\6#\0066\a\2\0009\a\"\a9\a%\a6\t\2\0009\t\"\t9\t#\t9\t.\t5\n/\0=\4,\nB\a\3\2=\a-\0066\6\0\0'\b0\0B\6\2\0026\a\0\0'\t1\0B\a\2\0023\b2\0003\t3\0006\n4\0005\f7\0005\r5\0\18\14\5\0B\14\1\2=\0146\r=\r8\f5\r9\0\18\14\5\0B\14\1\2=\0146\r=\r:\f5\r;\0\18\14\5\0B\14\1\2=\0146\r=\r<\f5\r=\0004\14\3\0>\2\1\14=\14>\r\18\14\5\0B\14\1\2=\0146\r=\r?\f5\r@\0\18\14\5\0B\14\1\2=\0146\r=\rA\f5\rB\0\18\14\5\0B\14\1\2=\0146\r=\rC\f5\rF\0006\14\0\0'\16D\0B\14\2\0029\14E\14=\14E\r3\14G\0=\0146\r3\14H\0=\14I\r=\rJ\f3\rK\0B\r\1\2=\rL\fB\n\2\4H\r\4�8\15\r\0069\15M\15\18\17\14\0B\15\2\1F\r\3\3R\r�2\0\0�K\0\1\0\nsetup\16sumneko_lua\0\rtsserver\rroot_dir\0\0\1\0\0\17init_options\22nvim-lsp-ts-utils\veslint\1\0\0\fpyright\1\0\0\bvls\bcmd\1\0\0\18rust_analyzer\1\0\0\14ansiblels\1\0\0\vbashls\1\0\0\14on_attach\1\0\0\npairs\0\0\19lspconfig.util\14lspconfig\1\0\0\19signature_help\31textDocument/signatureHelp\vborder\1\0\0\nhover\23textDocument/hover\1\0\3\nsigns\2\14underline\2\17virtual_text\2\27on_publish_diagnostics\15diagnostic\twith$textDocument/publishDiagnostics\rhandlers\blsp\0\1\3\0\0\b⡇\18LspBorderLeft\1\3\0\0\b⠉\20LspBorderBottom\1\3\0\0\b⠉\20LspBorderBottom\1\3\0\0\b⠉\20LspBorderBottom\1\3\0\0\b⢸\19LspBorderRight\1\3\0\0\b⣀\17LspBorderTop\1\3\0\0\b⣀\17LspBorderTop\1\3\0\0\b⣀\17LspBorderTop�\1        hi LspBorderTop guifg=#5d9794 guibg=#2e3440\n        hi LspBorderLeft guifg=#5d9794 guibg=#3b4252\n        hi LspBorderRight guifg=#5d9794 guibg=#3b4252\n        hi LspBorderBottom guifg=#5d9794 guibg=#2e3440\n      \14nvim_exec\bapi\0\22ReloadLSPSettings\0\20ShowLSPSettings\a_G\1\0\2\ttext\t🌵\vtexthl\23DiagnosticSignHint\23DiagnosticSignHint\1\0\2\ttext\t🌵\vtexthl\23DiagnosticSignInfo\23DiagnosticSignInfo\1\0\2\ttext\t🍯\vtexthl\23DiagnosticSignWarn\23DiagnosticSignWarn\1\0\2\ttext\t🔮\vtexthl\24DiagnosticSignError\24DiagnosticSignError\16sign_define\afn\r/bin/vls\19/bin/build/vls\15os_homedir\tloop\bvim\nmappy\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-lspconfig",
    url = "https://github.com/neovim/nvim-lspconfig"
  },
  ["nvim-treesitter"] = {
    config = { "\27LJ\2\n�\n\0\0\a\0004\0F6\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\0025\1\a\0005\2\4\0005\3\5\0=\3\6\2=\2\b\1=\1\3\0006\1\0\0'\3\t\0B\1\2\0029\1\n\1B\1\1\0016\1\0\0'\3\v\0B\1\2\0029\1\f\0015\3\18\0005\4\r\0005\5\14\0=\5\15\0045\5\16\0=\5\17\4=\4\19\0035\4\20\0005\5\21\0=\5\22\4=\4\23\0035\4\24\0=\4\25\0035\4\26\0=\4\27\0035\4\30\0005\5\28\0005\6\29\0=\6\22\5=\5\31\4=\4 \0035\4!\0=\4\"\0035\4#\0=\4$\0035\4%\0=\4&\3B\1\2\0016\1\0\0'\3\t\0B\1\2\0029\1\f\0015\3(\0005\4'\0=\4)\0035\4*\0=\4+\0035\4-\0005\5,\0=\5.\4=\4/\3B\1\2\0016\1\0\0'\0030\0B\1\2\0029\0011\1'\0032\0'\0043\0B\1\3\1K\0\1\0,<Cmd>TSHighlightCapturesUnderCursor<CR>\r<Space>h\rnnoremap\nmappy\rmappings\vglobal\1\0\0\1\0\2\15org_agenda\age\16org_capture\agt\21org_agenda_files\1\2\0\0\f~/m/vim\27org_todo_keyword_faces\1\0\1\27org_default_notes_file\24~/m/vim/default.org\1\0\3\tTODO(:background #000000 :foreground red\14DELEGATED4:background #FFFFFF :slant italic :underline on\fWAITING\":foreground blue :weight bold\frainbow\1\0\1\venable\2\fautotag\1\0\1\venable\2\26context_commentstring\1\0\1\venable\2\rrefactor\17smart_rename\1\0\0\1\0\1\17smart_rename\agr\1\0\1\venable\2\21ensure_installed\1\23\0\0\borg\15commonlisp\15dockerfile\nlatex\rmarkdown\tjson\bcss\15typescript\15javascript\thtml\btsx\tyaml\trust\vpython\tbash\6v\blua\nregex\bvue\fgraphql\ttoml\ago\vindent\1\0\1\venable\2\26incremental_selection\fkeymaps\1\0\3\21node_decremental\6,\19init_selection\agi\21node_incremental\6.\1\0\1\venable\2\14highlight\1\0\0&additional_vim_regex_highlighting\1\2\0\0\borg\fdisable\1\2\0\0\borg\1\0\1\venable\2\nsetup\28nvim-treesitter.configs\21setup_ts_grammar\forgmode\17install_info\1\0\1\rfiletype\nvlang\nfiles\1\2\0\0\17src/parser.c\1\0\2\burl-https://github.com/nedpals/tree-sitter-v\rrevision\vmaster\6v\23get_parser_configs\28nvim-treesitter.parsers\frequire\0" },
    load_after = {
      ["nvim-treesitter-context"] = true,
      ["nvim-treesitter-refactor"] = true,
      ["nvim-ts-autotag"] = true,
      ["nvim-ts-context-commentstring"] = true,
      ["nvim-ts-rainbow"] = true,
      ["treesitter-unit"] = true
    },
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-treesitter",
    url = "https://github.com/nvim-treesitter/nvim-treesitter"
  },
  ["nvim-treesitter-context"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-treesitter-context",
    url = "https://github.com/romgrk/nvim-treesitter-context"
  },
  ["nvim-treesitter-refactor"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-treesitter-refactor",
    url = "https://github.com/nvim-treesitter/nvim-treesitter-refactor"
  },
  ["nvim-ts-autotag"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-ts-autotag",
    url = "https://github.com/windwp/nvim-ts-autotag"
  },
  ["nvim-ts-context-commentstring"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-ts-context-commentstring",
    url = "https://github.com/JoosepAlviste/nvim-ts-context-commentstring"
  },
  ["nvim-ts-rainbow"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-ts-rainbow",
    url = "https://github.com/p00f/nvim-ts-rainbow"
  },
  ["nvim-web-devicons"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/nvim-web-devicons",
    url = "https://github.com/kyazdani42/nvim-web-devicons"
  },
  ["org-bullets.nvim"] = {
    config = { "\27LJ\2\nd\0\0\4\0\6\0\t6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\2B\0\2\1K\0\1\0\fsymbols\1\0\0\1\5\0\0\b◉\b○\b✸\b✿\nsetup\16org-bullets\frequire\0" },
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/org-bullets.nvim",
    url = "https://github.com/akinsho/org-bullets.nvim"
  },
  ["orgmode.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/orgmode.nvim",
    url = "https://github.com/kristijanhusak/orgmode.nvim"
  },
  ["packer.nvim"] = {
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/packer.nvim",
    url = "https://github.com/wbthomason/packer.nvim"
  },
  ["plenary.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/plenary.nvim",
    url = "https://github.com/nvim-lua/plenary.nvim"
  },
  sniprun = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/sniprun",
    url = "https://github.com/michaelb/sniprun"
  },
  ["telescope-fzf-native.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/telescope-fzf-native.nvim",
    url = "https://github.com/nvim-telescope/telescope-fzf-native.nvim"
  },
  ["telescope-media-files.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/telescope-media-files.nvim",
    url = "https://github.com/nvim-telescope/telescope-media-files.nvim"
  },
  ["telescope.nvim"] = {
    config = { "\27LJ\2\nM\0\0\b\2\2\0\v6\0\0\0-\2\0\0B\0\2\4X\3\4�-\5\1\0009\5\1\5\18\a\4\0B\5\2\1E\3\3\3R\3�K\0\1\0\2�\1�\19load_extension\vipairs�\f\1\0\t\0008\1^6\0\0\0006\2\1\0'\3\2\0B\0\3\3\14\0\0\0X\2\1�2\0V�9\2\3\0015\4$\0005\5\5\0005\6\4\0=\6\6\0055\6\b\0005\a\a\0=\a\t\0065\a\n\0=\a\v\6=\6\f\0056\6\1\0'\b\r\0B\6\2\0029\6\14\6=\6\15\0055\6\16\0=\6\17\0056\6\1\0'\b\r\0B\6\2\0029\6\18\6=\6\19\0055\6\20\0=\6\21\0054\6\0\0=\6\22\0055\6\23\0=\6\24\0055\6\25\0=\6\26\0056\6\1\0'\b\27\0B\6\2\0029\6\28\0069\6\29\6=\6\30\0056\6\1\0'\b\27\0B\6\2\0029\6\31\0069\6\29\6=\6 \0056\6\1\0'\b\27\0B\6\2\0029\6!\0069\6\29\6=\6\"\0056\6\1\0'\b\27\0B\6\2\0029\6#\6=\6#\5=\5%\0045\5'\0005\6&\0=\6(\0055\6*\0005\a)\0=\a+\6=\6,\5=\5-\4B\2\2\0015\2.\0'\3/\0006\0040\0009\0041\0049\0042\4'\0063\0B\4\2\2\t\4\0\0X\4\b�6\0044\0009\0045\4\18\6\2\0'\a,\0B\4\3\1\18\4\3\0'\0056\0&\3\5\0046\4\0\0003\0067\0B\4\2\0012\0\0�K\0\1\0K\0\1\0\0#, \"telescope-media-files.nvim\"\vinsert\ntable\rueberzug\15executable\afn\bvim.\"extensions\", \"telescope-fzf-native.nvim\"\1\4\0\0\vthemes\nterms\bfzf\15extensions\16media_files\14filetypes\1\0\1\rfind_cmd\arg\1\5\0\0\bpng\twebp\bjpg\tjpeg\bfzf\1\0\0\1\0\4\14case_mode\15smart_case\25override_file_sorter\2\nfuzzy\2\28override_generic_sorter\1\rdefaults\1\0\0\27buffer_previewer_maker\21qflist_previewer\22vim_buffer_qflist\19grep_previewer\23vim_buffer_vimgrep\19file_previewer\bnew\19vim_buffer_cat\25telescope.previewers\fset_env\1\0\1\14COLORTERM\14truecolor\16borderchars\1\t\0\0\b─\b│\b─\b│\b╭\b╮\b╯\b╰\vborder\17path_display\1\2\0\0\rabsolute\19generic_sorter\29get_generic_fuzzy_sorter\25file_ignore_patterns\1\2\0\0\17node_modules\16file_sorter\19get_fuzzy_file\22telescope.sorters\18layout_config\rvertical\1\0\1\vmirror\1\15horizontal\1\0\3\vheight\4����\t����\3\19preview_cutoff\3x\nwidth\4����\3����\3\1\0\3\20prompt_position\btop\18results_width\4����\t����\3\18preview_width\4����\t����\3\22vimgrep_arguments\1\0\n\rwinblend\3\0\23selection_strategy\nreset\18prompt_prefix\v   \17entry_prefix\a  \21sorting_strategy\14ascending\19color_devicons\2\ruse_less\2\17initial_mode\vinsert\20layout_strategy\15horizontal\20selection_caret\a  \1\b\0\0\arg\18--color=never\17--no-heading\20--with-filename\18--line-number\r--column\17--smart-case\nsetup\14telescope\frequire\npcall\2\0" },
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/telescope.nvim",
    url = "https://github.com/nvim-telescope/telescope.nvim"
  },
  ["treesitter-unit"] = {
    after = { "nvim-treesitter" },
    config = { "\27LJ\2\nZ\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0'\2\3\0B\0\2\1K\0\1\0\15CursorLine\24toggle_highlighting\20treesitter-unit\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/treesitter-unit",
    url = "https://github.com/David-Kunz/treesitter-unit"
  },
  ["trouble.nvim"] = {
    commands = { "TroubleToggle", "Trouble" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/trouble.nvim",
    url = "https://github.com/folke/trouble.nvim"
  },
  ["v-vim"] = {
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/v-vim",
    url = "https://github.com/ollykel/v-vim"
  },
  ["vi-viz"] = {
    config = { "\27LJ\2\n�\1\0\4\v\0\a\0\0246\4\0\0009\4\1\0049\4\2\4\18\6\0\0\18\a\1\0\18\b\2\0005\t\3\0009\n\4\3\14\0\n\0X\v\1�+\n\1\0=\n\4\t9\n\5\3\14\0\n\0X\v\1�+\n\1\0=\n\5\t9\n\6\3\14\0\n\0X\v\1�+\n\1\0=\n\6\tB\4\5\1K\0\1\0\vscript\texpr\vsilent\1\0\1\fnoremap\2\20nvim_set_keymap\bapi\bvim�\5\1\0\b\0\21\00095\0\0\0003\1\1\0\18\2\1\0'\4\2\0'\5\3\0'\6\4\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\3\0'\6\6\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\a\0'\6\b\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\t\0'\6\n\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\v\0'\6\f\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\r\0'\6\14\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\15\0'\6\16\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\17\0'\6\18\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\19\0'\6\20\0\18\a\0\0B\2\5\1K\0\1\0/<cmd>lua require('vi-viz').vizAppend()<CR>\aaa/<cmd>lua require('vi-viz').vizInsert()<CR>\aii/<cmd>lua require('vi-viz').vizChange()<CR>\6c0<cmd>lua require('vi-viz').vizPattern()<CR>\6r5<cmd>lua require('vi-viz').vizContract1Chr()<CR>\6-3<cmd>lua require('vi-viz').vizExpand1Chr()<CR>\6=1<cmd>lua require('vi-viz').vizContract()<CR>\v<left>/<cmd>lua require('vi-viz').vizExpand()<CR>\6x-<cmd>lua require('vi-viz').vizInit()<CR>\f<right>\6n\0\1\0\1\vsilent\2\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/vi-viz",
    url = "https://github.com/olambo/vi-viz"
  },
  ["vim-tmux-runner"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/vim-tmux-runner",
    url = "https://github.com/christoomey/vim-tmux-runner"
  }
}

time([[Defining packer_plugins]], false)
-- Config for: telescope.nvim
time([[Config for telescope.nvim]], true)
try_loadstring("\27LJ\2\nM\0\0\b\2\2\0\v6\0\0\0-\2\0\0B\0\2\4X\3\4�-\5\1\0009\5\1\5\18\a\4\0B\5\2\1E\3\3\3R\3�K\0\1\0\2�\1�\19load_extension\vipairs�\f\1\0\t\0008\1^6\0\0\0006\2\1\0'\3\2\0B\0\3\3\14\0\0\0X\2\1�2\0V�9\2\3\0015\4$\0005\5\5\0005\6\4\0=\6\6\0055\6\b\0005\a\a\0=\a\t\0065\a\n\0=\a\v\6=\6\f\0056\6\1\0'\b\r\0B\6\2\0029\6\14\6=\6\15\0055\6\16\0=\6\17\0056\6\1\0'\b\r\0B\6\2\0029\6\18\6=\6\19\0055\6\20\0=\6\21\0054\6\0\0=\6\22\0055\6\23\0=\6\24\0055\6\25\0=\6\26\0056\6\1\0'\b\27\0B\6\2\0029\6\28\0069\6\29\6=\6\30\0056\6\1\0'\b\27\0B\6\2\0029\6\31\0069\6\29\6=\6 \0056\6\1\0'\b\27\0B\6\2\0029\6!\0069\6\29\6=\6\"\0056\6\1\0'\b\27\0B\6\2\0029\6#\6=\6#\5=\5%\0045\5'\0005\6&\0=\6(\0055\6*\0005\a)\0=\a+\6=\6,\5=\5-\4B\2\2\0015\2.\0'\3/\0006\0040\0009\0041\0049\0042\4'\0063\0B\4\2\2\t\4\0\0X\4\b�6\0044\0009\0045\4\18\6\2\0'\a,\0B\4\3\1\18\4\3\0'\0056\0&\3\5\0046\4\0\0003\0067\0B\4\2\0012\0\0�K\0\1\0K\0\1\0\0#, \"telescope-media-files.nvim\"\vinsert\ntable\rueberzug\15executable\afn\bvim.\"extensions\", \"telescope-fzf-native.nvim\"\1\4\0\0\vthemes\nterms\bfzf\15extensions\16media_files\14filetypes\1\0\1\rfind_cmd\arg\1\5\0\0\bpng\twebp\bjpg\tjpeg\bfzf\1\0\0\1\0\4\14case_mode\15smart_case\25override_file_sorter\2\nfuzzy\2\28override_generic_sorter\1\rdefaults\1\0\0\27buffer_previewer_maker\21qflist_previewer\22vim_buffer_qflist\19grep_previewer\23vim_buffer_vimgrep\19file_previewer\bnew\19vim_buffer_cat\25telescope.previewers\fset_env\1\0\1\14COLORTERM\14truecolor\16borderchars\1\t\0\0\b─\b│\b─\b│\b╭\b╮\b╯\b╰\vborder\17path_display\1\2\0\0\rabsolute\19generic_sorter\29get_generic_fuzzy_sorter\25file_ignore_patterns\1\2\0\0\17node_modules\16file_sorter\19get_fuzzy_file\22telescope.sorters\18layout_config\rvertical\1\0\1\vmirror\1\15horizontal\1\0\3\vheight\4����\t����\3\19preview_cutoff\3x\nwidth\4����\3����\3\1\0\3\20prompt_position\btop\18results_width\4����\t����\3\18preview_width\4����\t����\3\22vimgrep_arguments\1\0\n\rwinblend\3\0\23selection_strategy\nreset\18prompt_prefix\v   \17entry_prefix\a  \21sorting_strategy\14ascending\19color_devicons\2\ruse_less\2\17initial_mode\vinsert\20layout_strategy\15horizontal\20selection_caret\a  \1\b\0\0\arg\18--color=never\17--no-heading\20--with-filename\18--line-number\r--column\17--smart-case\nsetup\14telescope\frequire\npcall\2\0", "config", "telescope.nvim")
time([[Config for telescope.nvim]], false)
-- Config for: null-ls.nvim
time([[Config for null-ls.nvim]], true)
try_loadstring("\27LJ\2\n�\5\0\0\t\0#\2V6\0\0\0'\2\1\0B\0\2\0024\1\r\0009\2\2\0009\2\3\0029\2\4\2>\2\1\0019\2\2\0009\2\3\0029\2\5\2>\2\2\0019\2\2\0009\2\3\0029\2\6\2>\2\3\0019\2\2\0009\2\3\0029\2\a\2>\2\4\0019\2\2\0009\2\b\0029\2\t\2>\2\5\0019\2\2\0009\2\3\0029\2\n\2>\2\6\0019\2\2\0009\2\3\0029\2\v\2>\2\a\0019\2\2\0009\2\b\0029\2\f\2>\2\b\0019\2\2\0009\2\3\0029\2\r\2>\2\t\0019\2\2\0009\2\3\0029\2\14\0029\2\15\0025\4\17\0005\5\16\0=\5\18\4B\2\2\2>\2\n\0019\2\2\0009\2\b\0029\2\19\0029\2\15\0025\4\21\0005\5\20\0=\5\18\0045\5\22\0006\6\23\0009\6\24\0069\6\25\6'\b\26\0B\6\2\0?\6\0\0=\5\27\4B\2\2\2>\2\v\0019\2\2\0009\2\b\0029\2\28\0029\2\15\0025\4\31\0005\5\29\0006\6\23\0009\6\24\0069\6\25\6'\b\30\0B\6\2\0?\6\0\0=\5\27\4B\2\2\0?\2\1\0009\2 \0005\4!\0=\1\"\4B\2\2\1K\0\1\0\fsources\1\0\0\nsetup\1\0\0#~/.config/null-ls/yamllint.yml\1\2\0\0\18--config-file\ryamllint\15extra_args\"~/.config/null-ls/selene.toml\vexpand\afn\bvim\1\2\0\0\r--config\1\0\0\1\2\0\0\blua\vselene\14filetypes\1\0\0\1\t\0\0\thtml\tjson\tyaml\rmarkdown\fgraphql\bcss\tscss\tless\twith\14prettierd\16shellharden\15shellcheck\nshfmt\frustfmt\vpylint\16diagnostics\27reorder_python_imports\nblack\vstylua\nguile\15formatting\rbuiltins\fnull-ls\frequire\5����\4\25����\4\0", "config", "null-ls.nvim")
time([[Config for null-ls.nvim]], false)

-- Command lazy-loads
time([[Defining lazy-load commands]], true)
pcall(vim.cmd, [[command -nargs=* -range -bang -complete=file Trouble lua require("packer.load")({'trouble.nvim'}, { cmd = "Trouble", l1 = <line1>, l2 = <line2>, bang = <q-bang>, args = <q-args>, mods = "<mods>" }, _G.packer_plugins)]])
pcall(vim.cmd, [[command -nargs=* -range -bang -complete=file TroubleToggle lua require("packer.load")({'trouble.nvim'}, { cmd = "TroubleToggle", l1 = <line1>, l2 = <line2>, bang = <q-bang>, args = <q-args>, mods = "<mods>" }, _G.packer_plugins)]])
time([[Defining lazy-load commands]], false)

vim.cmd [[augroup packer_load_aucmds]]
vim.cmd [[au!]]
  -- Filetype lazy-loads
time([[Defining lazy-load filetype autocommands]], true)
vim.cmd [[au FileType rust ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "rust" }, _G.packer_plugins)]]
vim.cmd [[au FileType yaml ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "yaml" }, _G.packer_plugins)]]
vim.cmd [[au FileType lua ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "lua" }, _G.packer_plugins)]]
vim.cmd [[au FileType ion ++once lua require("packer.load")({'elvish.vim', 'ion-vim'}, { ft = "ion" }, _G.packer_plugins)]]
vim.cmd [[au FileType sh ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "sh" }, _G.packer_plugins)]]
vim.cmd [[au FileType vlang ++once lua require("packer.load")({'nvim-lspconfig', 'v-vim'}, { ft = "vlang" }, _G.packer_plugins)]]
vim.cmd [[au FileType python ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "python" }, _G.packer_plugins)]]
vim.cmd [[au FileType css ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "css" }, _G.packer_plugins)]]
vim.cmd [[au FileType javascript ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "javascript" }, _G.packer_plugins)]]
vim.cmd [[au FileType javascriptreact ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "javascriptreact" }, _G.packer_plugins)]]
vim.cmd [[au FileType typescript ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "typescript" }, _G.packer_plugins)]]
vim.cmd [[au FileType typescriptreact ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "typescriptreact" }, _G.packer_plugins)]]
vim.cmd [[au FileType vue ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "vue" }, _G.packer_plugins)]]
vim.cmd [[au FileType dockerfile ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "dockerfile" }, _G.packer_plugins)]]
vim.cmd [[au FileType org ++once lua require("packer.load")({'org-bullets.nvim'}, { ft = "org" }, _G.packer_plugins)]]
vim.cmd [[au FileType json ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "json" }, _G.packer_plugins)]]
time([[Defining lazy-load filetype autocommands]], false)
  -- Event lazy-loads
time([[Defining lazy-load event autocommands]], true)
vim.cmd [[au InsertEnter * ++once lua require("packer.load")({'nvim-autopairs', 'lspkind-nvim', 'friendly-snippets'}, { event = "InsertEnter *" }, _G.packer_plugins)]]
vim.cmd [[au BufEnter * ++once lua require("packer.load")({'elvish.vim', 'v-vim', 'cokeline.nvim', 'lightspeed.nvim', 'org-bullets.nvim', 'ion-vim', 'indent-blankline.nvim'}, { event = "BufEnter *" }, _G.packer_plugins)]]
vim.cmd [[au CursorHold * ++once lua require("packer.load")({'nvim-lspconfig', 'trouble.nvim'}, { event = "CursorHold *" }, _G.packer_plugins)]]
vim.cmd [[au BufRead * ++once lua require("packer.load")({'nvim-ts-context-commentstring', 'gitsigns.nvim', 'nvim-treesitter-context', 'nvim-comment', 'nvim-colorizer.lua', 'nvim-ts-autotag', 'treesitter-unit', 'nvim-treesitter-refactor', 'vi-viz', 'fm-nvim', 'nvim-treesitter', 'nvim-ts-rainbow', 'vim-tmux-runner'}, { event = "BufRead *" }, _G.packer_plugins)]]
vim.cmd [[au FocusLost * ++once lua require("packer.load")({'nvim-lspconfig', 'trouble.nvim'}, { event = "FocusLost *" }, _G.packer_plugins)]]
vim.cmd [[au BufNewFile * ++once lua require("packer.load")({'nvim-ts-context-commentstring', 'nvim-treesitter-context', 'nvim-ts-autotag', 'treesitter-unit', 'nvim-treesitter-refactor', 'nvim-treesitter', 'nvim-ts-rainbow'}, { event = "BufNewFile *" }, _G.packer_plugins)]]
vim.cmd [[au BufReadPre * ++once lua require("packer.load")({'gruvbox-flat.nvim', 'lualine.nvim'}, { event = "BufReadPre *" }, _G.packer_plugins)]]
time([[Defining lazy-load event autocommands]], false)
vim.cmd("augroup END")
vim.cmd [[augroup filetypedetect]]
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/elvish.vim/ftdetect/elvish.vim]], true)
vim.cmd [[source /home/ice/.local/share/nvim/site/pack/packer/opt/elvish.vim/ftdetect/elvish.vim]]
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/elvish.vim/ftdetect/elvish.vim]], false)
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/v-vim/ftdetect/vlang.vim]], true)
vim.cmd [[source /home/ice/.local/share/nvim/site/pack/packer/opt/v-vim/ftdetect/vlang.vim]]
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/v-vim/ftdetect/vlang.vim]], false)
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/ion-vim/ftdetect/ion.vim]], true)
vim.cmd [[source /home/ice/.local/share/nvim/site/pack/packer/opt/ion-vim/ftdetect/ion.vim]]
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/ion-vim/ftdetect/ion.vim]], false)
vim.cmd("augroup END")
if should_profile then save_profiles(1) end

end)

if not no_errors then
  error_msg = error_msg:gsub('"', '\\"')
  vim.api.nvim_command('echohl ErrorMsg | echom "Error in packer_compiled: '..error_msg..'" | echom "Please check your config for correctness" | echohl None')
end
