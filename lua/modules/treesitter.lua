return {
	{
		"kristijanhusak/orgmode.nvim",
		-- requires = {
		-- 	{
		-- 		"lukas-reineke/headlines.nvim",
		-- 		ft = "org",
		-- 		event = "BufEnter",
		-- 	},
	},
	{
		"akinsho/org-bullets.nvim",
		ft = "org",
		event = "BufEnter",
		config = function()
			require("org-bullets").setup({
				symbols = { "◉", "○", "✸", "✿" },
			})
		end,
	},
	{ "romgrk/nvim-treesitter-context", event = { "BufNewFile", "BufRead" } },
	{ "nvim-treesitter/nvim-treesitter-refactor", event = { "BufNewFile", "BufRead" } },
	{
		"David-Kunz/treesitter-unit",
		event = { "BufNewFile", "BufRead" },
		config = function()
			require("treesitter-unit").toggle_highlighting("CursorLine")
		end,
	},
	{ "JoosepAlviste/nvim-ts-context-commentstring", event = { "BufNewFile", "BufRead" } },
	{ "p00f/nvim-ts-rainbow", event = { "BufNewFile", "BufRead" } },
	{ "windwp/nvim-ts-autotag", event = { "BufNewFile", "BufRead" } },
	{ -- {{{ nvim-treesitter
		"nvim-treesitter/nvim-treesitter",
		event = { "BufNewFile", "BufRead" },
		after = {
			"nvim-treesitter-context",
			"nvim-treesitter-refactor",
			"treesitter-unit",
			"nvim-ts-context-commentstring",
			"nvim-ts-rainbow",
			"nvim-ts-autotag",
		},
		config = function()
			local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
			-- parser_config.org = {
			-- 	install_info = {
			-- 		url = "https://github.com/milisims/tree-sitter-org",
			-- 		-- revision = 'f110024d539e676f25b72b7c80b0fd43c34264ef',
			-- 		revision = "main",
			-- 		files = { "src/parser.c", "src/scanner.cc" },
			-- 	},
			-- 	filetype = "org",
			-- }
			parser_config.v = {
				install_info = {
					url = "https://github.com/nedpals/tree-sitter-v",
					revision = "master",
					files = { "src/parser.c" },
				},
				filetype = "vlang",
			}
			-- Load custom tree-sitter grammar for org filetype
			require("orgmode").setup_ts_grammar()
			require("nvim-treesitter.configs").setup({
				highlight = {
					enable = true,
					-- disable = { "perl", "org" },
					disable = { "org" },
					additional_vim_regex_highlighting = { "org" }, -- Required since TS highlighter doesn't support all syntax features (conceal)
				},
				incremental_selection = {
					enable = true,
					keymaps = {
						init_selection = "gi",
						node_incremental = ".",
						node_decremental = ",",
					},
				},
				indent = {
					enable = true,
				},
				ensure_installed = {
					"org",
					"commonlisp",
					"dockerfile",
					"latex",
					"markdown",
					"json",
					"css",
					"typescript",
					"javascript",
					"html",
					"tsx",
					"yaml",
					"rust",
					"python",
					"bash",
					"v",
					"lua",
					"regex",
					"vue",
					"graphql",
					"toml",
					"go",
				},
				-- TODO: disable because too slow in C
				refactor = {
					-- highlight_definitions = { enable = true },
					-- highlight_current_scope = { enable = true },
					smart_rename = {
						enable = true,
						keymaps = {
							smart_rename = "gr",
						},
					},
				},
				context_commentstring = {
					enable = true,
				},
				autotag = {
					enable = true,
				},
				rainbow = {
					enable = true,
				},
			})
			require("orgmode").setup({
				org_todo_keyword_faces = {
					WAITING = ":foreground blue :weight bold",
					DELEGATED = ":background #FFFFFF :slant italic :underline on",
					TODO = ":background #000000 :foreground red", -- overrides builtin color for `TODO` keyword
				},
				org_agenda_files = { "~/m/vim" },
				org_default_notes_file = "~/m/vim/default.org",
				mappings = {
					global = {
						org_agenda = "ge",
						org_capture = "gt",
					},
				},
			})
			require("mappy").nnoremap("<Space>h", "<Cmd>TSHighlightCapturesUnderCursor<CR>")
		end,
		run = ":TSUpdate",
	}, -- }}}
}
