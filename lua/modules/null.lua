return {
	{
		-- lazy load fail lspconfig requires
		"jose-elias-alvarez/null-ls.nvim",
		config = function()
			local null_ls = require("null-ls")
			local sources = {
				null_ls.builtins.formatting.guile,
				null_ls.builtins.formatting.stylua,
				null_ls.builtins.formatting.black,
				null_ls.builtins.formatting.reorder_python_imports,
				null_ls.builtins.diagnostics.pylint,
				null_ls.builtins.formatting.rustfmt,
				null_ls.builtins.formatting.shfmt,
				null_ls.builtins.diagnostics.shellcheck,
				null_ls.builtins.formatting.shellharden,
				null_ls.builtins.formatting.prettierd.with({
					filetypes = { "html", "json", "yaml", "markdown", "graphql", "css", "scss", "less" },
				}),
				null_ls.builtins.diagnostics.selene.with({
					filetypes = { "lua" },
					extra_args = { "--config", vim.fn.expand("~/.config/null-ls/selene.toml") },
				}),
				null_ls.builtins.diagnostics.yamllint.with({
					extra_args = { "--config-file", vim.fn.expand("~/.config/null-ls/yamllint.yml") },
				}),
				-- null_ls.builtins.code_actions.gitsigns,
			}
			null_ls.setup({
				-- debug = true,
				sources = sources,
			})
		end,
	},
	{
		"jose-elias-alvarez/nvim-lsp-ts-utils",
		-- ft = {
		-- 	"typescript",
		-- 	"typescriptreact",
		-- },
	},
}
