return {
	{
		"ray-x/lsp_signature.nvim",
		after = "nvim-lspconfig",
		config = function()
			local present, lspsignature = pcall(require, "lsp_signature")
			if present then
				lspsignature.setup({
					bind = true,
					doc_lines = 0,
					floating_window = true,
					fix_pos = true,
					hint_enable = true,
					hint_prefix = " ",
					hint_scheme = "String",
					hi_parameter = "Search",
					max_height = 22,
					max_width = 120, -- max_width of signature floating_window, line will be wrapped if exceed max_width
					handler_opts = {
						border = "single", -- double, single, shadow, none
					},
					zindex = 200, -- by default it will be on top of all floating windows, set to 50 send it to bottom
					padding = "", -- character to pad on left and right of signature can be ' ', or '|'  etc
				})
			end
		end,
	},
	{ -- {{{ nvim-lspconfig
		"neovim/nvim-lspconfig",
		opt = true,
		event = { "FocusLost", "CursorHold" },
		ft = {
			"lua",
			"json",
			"python",
			"vlang",
			"rust",
			"javascript",
			"javascriptreact",
			"typescript",
			"typescriptreact",
			"yaml",
			"vue",
			"dockerfile",
			"css",
			"sh",
		},
		config = function()
			local m = require("mappy")
			local vls_root_path = vim.loop.os_homedir() .. "/bin/build/vls"
			local vls_binary = vls_root_path .. "/bin/vls"

			-- define signs
			local sign_define = vim.fn.sign_define
			sign_define("DiagnosticSignError", { text = "🔮", texthl = "DiagnosticSignError" })
			sign_define("DiagnosticSignWarn", { text = "🍯", texthl = "DiagnosticSignWarn" })
			sign_define("DiagnosticSignInfo", { text = "🌵", texthl = "DiagnosticSignInfo" })
			sign_define("DiagnosticSignHint", { text = "🌵", texthl = "DiagnosticSignHint" })
			-- sign_define("DiagnosticSignError", { text = "", texthl = "DiagnosticSignError" })
			-- sign_define("DiagnosticSignWarn", { text = "", texthl = "DiagnosticSignWarn" })
			-- sign_define("DiagnosticSignInfo", { text = "", texthl = "DiagnosticSignInfo" })
			-- sign_define("DiagnosticSignHint", { text = "", texthl = "DiagnosticSignHint" })

			function _G.ShowLSPSettings()
				print(vim.inspect(vim.lsp.buf_get_clients()))
			end
			function _G.ReloadLSPSettings()
				vim.lsp.stop_client(vim.lsp.get_active_clients())
				vim.cmd([[edit]])
			end

			vim.api.nvim_exec(
				[[
        hi LspBorderTop guifg=#5d9794 guibg=#2e3440
        hi LspBorderLeft guifg=#5d9794 guibg=#3b4252
        hi LspBorderRight guifg=#5d9794 guibg=#3b4252
        hi LspBorderBottom guifg=#5d9794 guibg=#2e3440
      ]],
				false
			)
			local border = {
				{ "⣀", "LspBorderTop" },
				{ "⣀", "LspBorderTop" },
				{ "⣀", "LspBorderTop" },
				{ "⢸", "LspBorderRight" },
				{ "⠉", "LspBorderBottom" },
				{ "⠉", "LspBorderBottom" },
				{ "⠉", "LspBorderBottom" },
				{ "⡇", "LspBorderLeft" },
			}

			local lsp_on_attach = function(diag_maps_only)
				return function(client, bufnr)
					print(("LSP started: bufnr = %d"):format(bufnr))
					--require'completion'.on_attach()

					if client.config.flags then
						client.config.flags.allow_incremental_sync = true
					end

					vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

					-- ignore errors when executed multi times
					m.add_buffer_maps(function()
						m.bind("n", { "<A-J>", "<A-S-Ô>" }, function()
							vim.lsp.diagnostic.goto_next({
								popup_opts = { border = border },
							})
						end)
						m.bind("n", { "<A-K>", "<A-S->" }, function()
							vim.lsp.diagnostic.goto_prev({
								popup_opts = { border = border },
							})
						end)
						m.nnoremap("<Space>E", function()
							if vim.b.lsp_diagnostics_disabled then
								vim.lsp.diagnostic.enable()
							else
								vim.lsp.diagnostic.disable()
							end
							vim.b.lsp_diagnostics_disabled = not vim.b.lsp_diagnostics_disabled
						end)
						m.nnoremap("gl", function()
							-- vim.lsp.diagnostic.show_line_diagnostics({ border = border })
							-- vim.lsp.diagnostic.show_line_diagnostics({ border = border })
							vim.diagnostic.open_float()
						end)
						if not diag_maps_only then
							m.nnoremap("gk", vim.lsp.buf.hover)
							m.nnoremap("gd", vim.lsp.buf.type_definition)
							if vim.opt.filetype:get() ~= "help" then
								m.nnoremap("<C-]>", vim.lsp.buf.definition)
								m.nnoremap("<C-w><C-]>", function()
									vim.cmd([[split]])
									vim.lsp.buf.definition()
								end)
							end
							-- m.nnoremap("<C-x><C-k>", vim.lsp.buf.signature_help)
							-- m.nnoremap("g0", vim.lsp.buf.document_symbol)
							-- m.nnoremap("g=", vim.lsp.buf.formatting)
							m.nnoremap("ga", vim.lsp.buf.code_action)
							-- m.nnoremap("gi", vim.lsp.buf.implementation)
							m.nnoremap("gr", vim.lsp.buf.rename)
							-- m.nnoremap("gw", vim.lsp.buf.workspace_symbol)
							m.nnoremap("ge", vim.lsp.buf.declaration)
							-- m.nnoremap("gli", vim.lsp.buf.incoming_calls)
							-- m.nnoremap("glo", vim.lsp.buf.outgoing_calls)
							m.nnoremap("gf", vim.lsp.buf.references)
						end
					end)
				end
			end

			vim.lsp.handlers["textDocument/publishDiagnostics"] =
				vim.lsp.with(
					vim.lsp.diagnostic.on_publish_diagnostics,
					{
						underline = true,
						virtual_text = true,
						signs = true,
					}
				)
			vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = border })
			vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(
				vim.lsp.handlers.signature_help,
				{ border = border }
			)

			local lsp = require("lspconfig")
			local util = require("lspconfig.util")

			local function is_git_root(p)
				return util.path.is_dir(p) and util.path.exists(util.path.join(p, ".git"))
			end
			local function is_deno_dir(p)
				local base = p:gsub([[.*/]], "")
				for _, r in ipairs({
					[[^deno]],
					[[^ddc]],
					[[^cmp%-look$]],
					[[^neco%-vim$]],
					[[^git%-vines$]],
					[[^murus$]],
					[[^skkeleton$]],
				}) do
					if base:match(r) then
						return true
					end
				end
				return false
			end

			for name, config in pairs({
				-- clangd = { on_attach = lsp_on_attach() },
				-- cssls = { on_attach = lsp_on_attach() },
				-- dockerls = { on_attach = lsp_on_attach() },
				-- html = { on_attach = lsp_on_attach() },
				-- intelephense = { on_attach = lsp_on_attach() },
				--jsonls = {on_attach = lsp_on_attach()},
				--perlls = {on_attach = lsp_on_attach()},
				-- pyright = { on_attach = lsp_on_attach() },
				-- solargraph = { on_attach = lsp_on_attach() },
				-- sourcekit = { on_attach = lsp_on_attach() },
				-- terraformls = { on_attach = lsp_on_attach() },
				-- vimls = { on_attach = lsp_on_attach() },
				-- emmet_ls = { on_attach = lsp_on_attach() },
				-- emmet_ls = {
				-- 	on_attach = lsp_on_attach(),
				-- 	filetypes = { "html", "css", "blade", "javascriptreact", "typescriptreact" },
				-- },

				bashls = { on_attach = lsp_on_attach() },
				ansiblels = { on_attach = lsp_on_attach() },
				rust_analyzer = { on_attach = lsp_on_attach() },
				vls = { cmd = { vls_binary }, on_attach = lsp_on_attach() },
				pyright = { on_attach = lsp_on_attach() },
				-- racket_langserver = { on_attach = lsp_on_attach() },
				eslint = { on_attach = lsp_on_attach() },
				tsserver = {
					init_options = require("nvim-lsp-ts-utils").init_options,
					on_attach = function(client, bufnr)
						local ts_utils = require("nvim-lsp-ts-utils")

						-- defaults
						ts_utils.setup({
							debug = false,
							disable_commands = false,
							enable_import_on_completion = false,

							-- import all
							import_all_timeout = 5000, -- ms
							-- lower numbers = higher priority
							import_all_priorities = {
								same_file = 1, -- add to existing import statement
								local_files = 2, -- git files or files with relative path markers
								buffer_content = 3, -- loaded buffer content
								buffers = 4, -- loaded buffer names
							},
							import_all_scan_buffers = 100,
							import_all_select_source = false,

							-- filter diagnostics
							filter_out_diagnostics_by_severity = {},
							filter_out_diagnostics_by_code = {},

							-- inlay hints
							auto_inlay_hints = true,
							inlay_hints_highlight = "Comment",

							-- update imports on file move
							update_imports_on_move = false,
							require_confirmation_on_move = false,
							watch_dir = nil,
						})

						-- required to fix code action ranges and filter diagnostics
						ts_utils.setup_client(client)

						-- no default maps, so you may want to define some here
						local opts = { silent = true }
						-- vim.api.nvim_buf_set_keymap(bufnr, "n", "gs", ":TSLspOrganize<CR>", opts)
						-- vim.api.nvim_buf_set_keymap(bufnr, "n", "gr", ":TSLspRenameFile<CR>", opts)
						-- vim.api.nvim_buf_set_keymap(bufnr, "n", "gi", ":TSLspImportAll<CR>", opts)
					end,
					root_dir = function(startpath)
						return util.search_ancestors(startpath, function(p)
							return is_git_root(p) and not is_deno_dir(p)
						end)
					end,
				},

				sumneko_lua = (function()
					local sumneko_root_path = vim.loop.os_homedir() .. "/bin/build/lua-language-server"
					local sumneko_binary = ("%s/bin/%s/lua-language-server"):format(
						sumneko_root_path,
						vim.loop.os_uname().sysname == "Darwin" and "macOS" or "Linux"
					)
					return {
						on_attach = lsp_on_attach(),
						cmd = { sumneko_binary, "-E", sumneko_root_path .. "/main.lua" },
						settings = {
							Lua = {
								runtime = {
									version = "LuaJIT",
									path = vim.split(package.path, ";"),
								},
								completion = {
									keywordSnippet = "Disable",
								},
								diagnostics = {
									enable = true,
									globals = {
										"vim",
										"describe",
										"it",
										"before_each",
										"after_each",
										"packer_plugins",
										"hs",
									},
								},
								workspace = {
									library = {
										library = vim.api.nvim_get_runtime_file("", true),
										checkThirdParty = false,
									},
								},
								telemetry = { enable = false },
							},
						},
					}
				end)(),
			}) do
				lsp[name].setup(config)
			end
		end,
	}, -- }}}
}
