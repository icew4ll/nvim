local packer

local function init()
	if packer == nil then
		packer = require("packer")
		packer.init({
			compile_path = vim.fn.stdpath("config") .. "/lua/packer_compiled.lua",
			profile = {
				enable = false,
				threshold = 1,
			},
			disable_commands = true,
			max_jobs = 50,
			display = {
				open_fn = function(name)
					local last_win = vim.api.nvim_get_current_win()
					local last_pos = vim.api.nvim_win_get_cursor(last_win)

					local ok, win = pcall(function()
						vim.cmd([[packadd plenary.nvim]])
						return require("plenary.window.float").percentage_range_window(0.8, 0.8)
					end)

					if not ok then
						vim.cmd("65vnew [packer]")
						return true, vim.api.nvim_get_current_win(), vim.api.nvim_get_current_buf()
					end

					vim.api.nvim_buf_set_name(win.bufnr, name)
					vim.api.nvim_win_set_option(win.win_id, "winblend", 10)

					require("agrp").set({
						packer_wipe_out = {
							{
								"BufWipeout",
								"<buffer>",
								function()
									vim.api.nvim_set_current_win(last_win)
									vim.api.nvim_win_set_cursor(last_win, last_pos)
								end,
							},
						},
					})

					return true, win.win_id, win.bufnr
				end,
			},
		})
	end
	packer.reset()

	for _, name in pairs({
		"boot",
		"treesitter",
		"lsp",
		"null",
		"cmp",
		"ui",
		"utils",
		"syntax",
	}) do
		packer.use(require("modules." .. name))
	end
end

return setmetatable({}, {
	__index = function(_, key)
		init()
		return packer[key]
	end,
})
