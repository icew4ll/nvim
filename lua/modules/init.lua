function _G.run_packer(method, opts)
	vim.cmd([[packadd packer.nvim]])
	require("modules.load")[method](opts)
end

vim.cmd([[command! PackerInstall lua run_packer'install']])
vim.cmd([[command! PackerUpdate  lua run_packer'update']])
vim.cmd([[command! PackerSync    lua run_packer'sync']])
vim.cmd([[command! PackerClean   lua run_packer'clean']])
vim.cmd([[command! -nargs=* PackerCompile lua run_packer('compile', <q-args>)]])
vim.cmd([[command! PackerProfile lua run_packer'profile_output']])

require("agrp").set({
	packer_on_compile_done = {
		{
			"User",
			"PackerCompileDone",
			function()
				vim.notify = function(msg, _, opts)
					vim.api.nvim_echo({ { ("[%s] %s"):format(opts.title, msg), "WarningMsg" } }, true, {})
				end
			end,
		},
	},
})
