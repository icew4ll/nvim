return {
	-- {
	-- 	"rktjmp/lush.nvim",
	-- },
	-- {
	-- 	"olimorris/onedarkpro.nvim",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		require("onedarkpro").load()
	-- 	end,
	-- },
	-- {
	-- 	"projekt0n/github-nvim-theme",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		require("github-theme").setup()
	-- 	end,
	-- },
	-- {
	-- 	"rose-pine/neovim",
	-- 	as = "rose-pine",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		vim.api.nvim_command("colo rose-pine")
	-- 	end,
	-- },
	-- 	{
	-- 		"adisen99/codeschool.nvim",
	-- 		opt = true,
	-- 		event = "BufRead",
	-- 		config = function()
	-- 			vim.o.background = "dark" -- or "light" for light mode
	-- 			-- vim.g.codeschool_contrast_dark = "medium"
	-- 			-- Load and setup function to choose plugin and language highlights
	-- 			require("lush")(require("codeschool").setup({
	-- 				plugins = {
	-- 					"cmp", -- nvim-cmp
	-- 					"gitgutter",
	-- 					"gitsigns",
	-- 					"lsp",
	-- 					"lspsaga",
	-- 					"netrw",
	-- 					"neogit",
	-- 					"packer",
	-- 					"telescope",
	-- 					"treesitter",
	-- 				},
	-- 				langs = {
	-- 					"c",
	-- 					"clojure",
	-- 					"coffeescript",
	-- 					"csharp",
	-- 					"css",
	-- 					"elixir",
	-- 					"golang",
	-- 					"haskell",
	-- 					"html",
	-- 					"java",
	-- 					"js",
	-- 					"json",
	-- 					"jsx",
	-- 					"lua",
	-- 					"markdown",
	-- 					"moonscript",
	-- 					"objc",
	-- 					"ocaml",
	-- 					"purescript",
	-- 					"python",
	-- 					"ruby",
	-- 					"rust",
	-- 					"scala",
	-- 					"typescript",
	-- 					"viml",
	-- 					"xml",
	-- 				},
	-- 			}))
	-- 		end,
	-- 	},
	-- {
	-- 	"nxvu699134/vn-night.nvim",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		require("vn-night").setup()
	-- 	end,
	-- },
	-- {
	-- 	"yashguptaz/calvera-dark.nvim",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		-- Optional Example Configuration
	-- 		vim.g.calvera_italic_keywords = false
	-- 		vim.g.calvera_borders = true
	-- 		vim.g.calvera_contrast = true
	-- 		vim.g.calvera_hide_eob = true
	-- 		vim.g.calvera_custom_colors = { contrast = "#0f111a" }
	-- 		-- Required Setting
	-- 		require("calvera").set()
	-- 	end,
	-- },
	-- {
	-- 	"navarasu/onedark.nvim",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		require("onedark").load()
	-- 	end,
	-- },
	-- {
	-- 	"shaunsingh/moonlight.nvim",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		require("moonlight").set()
	-- 	end,
	-- },
	-- {
	-- 	"savq/melange",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		vim.api.nvim_command("colo melange")
	-- 	end,
	-- },
	-- {
	-- 	"tanvirtin/monokai.nvim",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		require("monokai").setup({ palette = require("monokai").pro })
	-- 	end,
	-- },
	-- {
	-- 	"marko-cerovac/material.nvim",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		vim.api.nvim_command("colo material")
	-- 		-- vim.g.material_style = "deep ocean"
	-- 		-- vim.g.material_style = "darker"
	-- 		require("material.functions").change_style("darker")
	-- 	end,
	-- },
	-- {
	-- 	"rafamadriz/neon",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		vim.api.nvim_command("colo neon")
	-- 	end,
	-- },
	-- {
	-- 	"EdenEast/nightfox.nvim",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		vim.api.nvim_command("colo nightfox")
	-- 	end,
	-- },
	{
		"eddyekofo94/gruvbox-flat.nvim",
		opt = true,
		event = "BufReadPre",
		config = function()
			vim.api.nvim_command("colo gruvbox-flat")
		end,
	},
	-- {
	-- 	"catppuccin/nvim",
	-- 	as = "catppuccin",
	-- 	opt = true,
	-- 	event = "BufRead",
	-- 	config = function()
	-- 		vim.api.nvim_command("colo catppuccin")
	-- 	end,
	-- },
	{
		"nvim-lualine/lualine.nvim",
		branch = "master",
		opt = true,
		event = "BufReadPre",
		config = function()
			local lualine = require("lualine")

			local colors = {
				-- bg = "#32302f",
				bg = "#192330",
				fg = "#bbc2cf",
				yellow = "#ECBE7B",
				cyan = "#008080",
				darkblue = "#081633",
				green = "#98be65",
				orange = "#FF8800",
				violet = "#a9a1e1",
				magenta = "#c678dd",
				blue = "#51afef",
				red = "#ec5f67",
			}

			local conditions = {
				buffer_not_empty = function()
					return vim.fn.empty(vim.fn.expand("%:t")) ~= 1
				end,
				hide_in_width = function()
					return vim.fn.winwidth(0) > 80
				end,
				check_git_workspace = function()
					local filepath = vim.fn.expand("%:p:h")
					local gitdir = vim.fn.finddir(".git", filepath .. ";")
					return gitdir and #gitdir > 0 and #gitdir < #filepath
				end,
			}

			-- Config
			local config = {
				options = {
					-- Disable sections and component separators
					component_separators = "",
					section_separators = "",
					theme = {
						-- We are going to use lualine_c an lualine_x as left and
						-- right section. Both are highlighted by c theme .  So we
						-- are just setting default looks o statusline
						normal = { c = { fg = colors.fg, bg = colors.bg } },
						inactive = { c = { fg = colors.fg, bg = colors.bg } },
					},
				},
				sections = {
					-- these are to remove the defaults
					lualine_a = {},
					lualine_b = {},
					lualine_y = {},
					lualine_z = {},
					-- These will be filled later
					lualine_c = {},
					lualine_x = {},
				},
				inactive_sections = {
					-- these are to remove the defaults
					lualine_a = {},
					lualine_v = {},
					lualine_y = {},
					lualine_z = {},
					lualine_c = {},
					lualine_x = {},
				},
			}

			-- Inserts a component in lualine_c at left section
			local function ins_left(component)
				table.insert(config.sections.lualine_c, component)
			end

			-- Inserts a component in lualine_x ot right section
			local function ins_right(component)
				table.insert(config.sections.lualine_x, component)
			end

			ins_left({
				-- mode component
				function()
					-- auto change color according to neovims mode
					local mode_color = {
						n = colors.red,
						i = colors.green,
						v = colors.blue,
						[""] = colors.blue,
						V = colors.blue,
						c = colors.magenta,
						no = colors.red,
						s = colors.orange,
						S = colors.orange,
						[""] = colors.orange,
						ic = colors.yellow,
						R = colors.violet,
						Rv = colors.violet,
						cv = colors.red,
						ce = colors.red,
						r = colors.cyan,
						rm = colors.cyan,
						["r?"] = colors.cyan,
						["!"] = colors.red,
						t = colors.red,
					}
					vim.api.nvim_command(
						"hi! LualineMode guifg=" .. mode_color[vim.fn.mode()] .. " guibg=" .. colors.bg
					)
					-- return "⚙️"
					return "🌙"
				end,
				color = "LualineMode",
				padding = { right = 1 },
			})

			ins_left({
				-- filesize component
				"filesize",
				cond = conditions.buffer_not_empty,
			})

			ins_left({
				"filename",
				cond = conditions.buffer_not_empty,
				color = { fg = colors.magenta, gui = "bold" },
			})

			ins_left({
				"diagnostics",
				sources = { "nvim_diagnostic" },
				symbols = { error = " ", warn = " ", info = " " },
				diagnostics_color = {
					color_error = { fg = colors.red },
					color_warn = { fg = colors.yellow },
					color_info = { fg = colors.cyan },
				},
			})

			ins_left({
				-- Lsp server name .
				function()
					local msg = "No Lsp"
					local buf_ft = vim.api.nvim_buf_get_option(0, "filetype")
					local clients = vim.lsp.get_active_clients()
					if next(clients) == nil then
						return msg
					end
					for _, client in ipairs(clients) do
						local filetypes = client.config.filetypes
						if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
							return client.name
						end
					end
					return msg
				end,
				icon = "📖",
				color = { fg = "#ffffff", gui = "bold" },
			})

			-- Add components to right sections
			ins_right({
				"o:encoding", -- option component same as &encoding in viml
				fmt = string.upper, -- I'm not sure why it's upper case either ;)
				cond = conditions.hide_in_width,
				color = { fg = colors.green, gui = "bold" },
			})

			ins_right({
				"fileformat",
				fmt = string.upper,
				icons_enabled = false, -- I think icons are cool but Eviline doesn't have them. sigh
				color = { fg = colors.green, gui = "bold" },
			})

			ins_right({
				"branch",
				icon = "",
				color = { fg = colors.violet, gui = "bold" },
			})

			ins_right({
				"diff",
				-- Is it me or the symbol for modified us really weird
				symbols = { added = " ", modified = "柳 ", removed = " " },
				diff_color = {
					added = { fg = colors.green },
					modified = { fg = colors.orange },
					removed = { fg = colors.red },
				},
				cond = conditions.hide_in_width,
			})

			ins_right({ "location" })

			-- Now don't forget to initialize lualine
			lualine.setup(config)
		end,
	},
	{
		"noib3/cokeline.nvim",
		event = "BufEnter",
		requires = "kyazdani42/nvim-web-devicons", -- If you want devicons
		config = function()
			local get_hex = require("cokeline/utils").get_hex

			require("cokeline").setup({
				default_hl = {
					fg = function(buffer)
						return buffer.is_focused and get_hex("ColorColumn", "bg") or get_hex("Normal", "fg")
					end,
					bg = function(buffer)
						return buffer.is_focused and get_hex("Normal", "fg") or get_hex("ColorColumn", "bg")
					end,
				},

				components = {
					{
						text = function(buffer)
							return " " .. buffer.devicon.icon
						end,
						fg = function(buffer)
							return buffer.devicon.color
						end,
					},
					{
						text = function(buffer)
							return buffer.unique_prefix
						end,
						fg = get_hex("Comment", "fg"),
						style = "italic",
					},
					{
						text = function(buffer)
							return buffer.filename .. " "
						end,
					},
					{
						text = "",
						delete_buffer_on_left_click = true,
					},
					{
						text = " ",
					},
				},
			})
		end,
	},
	{
		"lewis6991/gitsigns.nvim",
		opt = true,
		-- event = { "FocusLost", "CursorHold" },
		event = "BufRead",
		config = function()
			require("gitsigns").setup({
				signs = {
					add = { hl = "GitSignsAdd", text = "🌲" },
					change = { hl = "GitSignsChange", text = "🤎" },
					delete = { hl = "GitSignsDelete", text = "🪲" },
					topdelete = { hl = "GitSignsDelete", text = "🪲" },
					changedelete = { hl = "GitSignsChange", text = "🐝" },
				},
				-- numhl = true,
				numhl = true,
				current_line_blame = true,
				current_line_blame_opts = {
					delay = 10,
				},
				word_diff = false,
			})
		end,
	},
}
