return {
	{
		"nvim-telescope/telescope.nvim",
		-- opt = true,
		-- event = "BufReadPre",
		requires = {
			{
				"nvim-telescope/telescope-fzf-native.nvim",
				run = "make",
			},
			{
				"nvim-telescope/telescope-media-files.nvim",
			},
		},
		config = function()
			local present, telescope = pcall(require, "telescope")
			if not present then
				return
			end

			telescope.setup({
				defaults = {
					vimgrep_arguments = {
						"rg",
						"--color=never",
						"--no-heading",
						"--with-filename",
						"--line-number",
						"--column",
						"--smart-case",
					},
					prompt_prefix = "   ",
					selection_caret = "  ",
					entry_prefix = "  ",
					initial_mode = "insert",
					selection_strategy = "reset",
					sorting_strategy = "ascending",
					layout_strategy = "horizontal",
					layout_config = {
						horizontal = {
							prompt_position = "top",
							preview_width = 0.55,
							results_width = 0.8,
						},
						vertical = {
							mirror = false,
						},
						width = 0.87,
						height = 0.80,
						preview_cutoff = 120,
					},
					file_sorter = require("telescope.sorters").get_fuzzy_file,
					file_ignore_patterns = { "node_modules" },
					generic_sorter = require("telescope.sorters").get_generic_fuzzy_sorter,
					path_display = { "absolute" },
					winblend = 0,
					border = {},
					borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
					color_devicons = true,
					use_less = true,
					set_env = { ["COLORTERM"] = "truecolor" }, -- default = nil,
					file_previewer = require("telescope.previewers").vim_buffer_cat.new,
					grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
					qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
					-- Developer configurations: Not meant for general override
					buffer_previewer_maker = require("telescope.previewers").buffer_previewer_maker,
				},
				extensions = {
					fzf = {
						fuzzy = true, -- false will only do exact matching
						override_generic_sorter = false, -- override the generic sorter
						override_file_sorter = true, -- override the file sorter
						case_mode = "smart_case", -- or "ignore_case" or "respect_case"
						-- the default case_mode is "smart_case"
					},
					media_files = {
						filetypes = { "png", "webp", "jpg", "jpeg" },
						find_cmd = "rg", -- find command (defaults to `fd`)
					},
				},
			})

			local extensions = { "themes", "terms", "fzf" }
			local packer_repos = [["extensions", "telescope-fzf-native.nvim"]]

			if vim.fn.executable("ueberzug") == 1 then
				table.insert(extensions, "media_files")
				packer_repos = packer_repos .. ', "telescope-media-files.nvim"'
			end

			pcall(function()
				for _, ext in ipairs(extensions) do
					telescope.load_extension(ext)
				end
			end)
		end,
	},
	{
		"ggandor/lightspeed.nvim",
		opt = true,
		event = "BufEnter",
	},
	{
		"is0n/fm-nvim",
		opt = true,
		event = "BufRead",
	},
	{
		"christoomey/vim-tmux-runner",
		opt = true,
		event = "BufRead",
	},
	-- {
	-- 	"simrat39/symbols-outline.nvim",
	-- 	opt = true,
	-- 	event = "BufReadPre",
	-- },
	{
		"olambo/vi-viz",
		opt = true,
		event = "BufRead",
		-- event = "BufReadPre",
		config = function()
			local opts = { silent = true }

			local function map(mode, key, result, optz)
				vim.api.nvim_set_keymap(mode, key, result, {
					noremap = true,
					silent = optz.silent or false,
					expr = optz.expr or false,
					script = optz.script or false,
				})
			end
			map("n", "<right>", "<cmd>lua require('vi-viz').vizInit()<CR>", opts)
			-- expand and contract
			map("x", "<right>", "<cmd>lua require('vi-viz').vizExpand()<CR>", opts)
			map("x", "<left>", "<cmd>lua require('vi-viz').vizContract()<CR>", opts)
			-- expand and contract by 1 char either side
			map("x", "=", "<cmd>lua require('vi-viz').vizExpand1Chr()<CR>", opts)
			map("x", "-", "<cmd>lua require('vi-viz').vizContract1Chr()<CR>", opts)
			-- good use for the r key in visual mode
			map("x", "r", "<cmd>lua require('vi-viz').vizPattern()<CR>", opts)
			-- nice to have to get dot repeat on single words
			map("x", "c", "<cmd>lua require('vi-viz').vizChange()<CR>", opts)
			-- nice to have to insert before and after

			map("x", "ii", "<cmd>lua require('vi-viz').vizInsert()<CR>", opts)

			map("x", "aa", "<cmd>lua require('vi-viz').vizAppend()<CR>", opts)
		end,
	},
}
